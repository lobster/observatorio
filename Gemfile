source "https://rubygems.org"

ruby "2.6.5"

# Ruby on Rails
gem "rails",                            "~> 5.0"
# User's authorization
gem "cancancan",                        "~> 3.0"
# Files upload
gem "carrierwave",                      "~> 2.0"
# Upload images & videos to Cloudinary
gem "cloudinary",                       "~> 1.0"
# Validate CNPJ
gem "cnpj_validator",                   "< 1"
# Useful information about countries
gem "countries",                        "~> 3.0",  require: "countries/global"
# User management
gem "devise",                           "~> 4.0"
gem "devise-i18n",                      "~> 1.0"
# Generate string diffs
gem "differ",                           "< 1"
# HTTP requests
gem "excon",                            "< 1"
gem "faraday",                          "< 1"
gem "faraday_middleware",               "< 1"
# Local file storage
gem "fog-local",                        "< 1"
# OpenStack file storage
gem "fog-openstack",                    "~> 1.0"
# Optimized JSON parser
gem "oj",                               "~> 3.0"
# Track model changes
gem "paper_trail",                      "~> 10.0"
gem "paper_trail-association_tracking", "~> 2.0"
# Convert HTML to PDF
gem "pdfkit",                           "< 1"
# PostgreSQL client
gem "pg",                               "~> 1.0"
# Puma web server
gem "puma",                             "~> 4.0"
# JSON view templates
gem "rabl",                             "< 1"
# Internationalization
gem "rails-i18n",                       "~> 5.0"
# Spredsheets reader
gem "roo",                              "~> 2.0"
# SASS for Rails https://sass-lang.com/
gem "sassc-rails",                      "~> 2.0"
# Asynchronous processing
gem "sucker_punch",                     "~> 2.0"

group :development do
  gem "listen",                         "~> 3.0"
end

group :development, :test do
  gem "byebug"
end

group :test do
  gem "database_cleaner",               "~> 1.0"
  gem "factory_bot_rails",              "~> 5.0"
  gem "rails-controller-testing",       "~> 1.0"
  gem "rspec-collection_matchers",      "~> 1.0"
  gem "rspec-rails",                    "~> 3.0"
  gem "shoulda-context",                "~> 1.0"
  gem "shoulda-matchers",               "~> 4.0"
  gem "simplecov",                      "< 1",     require: false
  gem "tzinfo-data",                    "~> 1.0" # Required in alpine Linux
end
