class Geocoder
  ServiceError = Class.new(StandardError)
  GeocodingError = Class.new(StandardError)

  NOMINATIM_API = Faraday.new("https://nominatim.openstreetmap.org/") do |faraday|
    faraday.response :json, content_type: "application/json"
    faraday.adapter Faraday.default_adapter
  end

  def initialize(default_params = {})
    @default_params = default_params
  end

  def reverse(params)
    processed_params = process_params(params)
    response = NOMINATIM_API.get("/reverse", processed_params)
    unless response.success?
      raise ServiceError, "Error geocoding #{processed_params.to_json}"
    end
    if response.body["error"].present?
      raise GeocodingError, response.body["error"]
    end
    process_response(response.body, processed_params)
  end

  private

  def process_params(params)
    param = params.clone
    params = @default_params.merge(params).symbolize_keys
    params[:lon] ||= params.delete(:lng) if params.key?(:lng)
    params[:"accept-language"] ||= params.delete(:language) if params.key?(:language)
    if params[:osm_type] == "node"
      params[:osm_type] = "N"
    end
    if params[:osm_type] == "relation"
      params[:osm_type] = "R"
    end
    if params[:osm_type] == "way"
      params[:osm_type] = "W"
    end
    params
  end

  def process_response(geocoding_response, params)
    geocoding_response["lng"] = geocoding_response.delete("lon")
    if params[:lat] && params[:lon]
      geocoding_response["lat"] = params[:lat]
      geocoding_response["lng"] = params[:lon]
      geocoding_response["geojson"] = {
        "type" => "Point",
        "coordinates" => [params[:lon], params[:lat]]
      }
    end
    if geocoding_response["geojson"] && geocoding_response["geojson"]["type"] == "Point"
      geocoding_response.delete("boundingbox")
    end
    if geocoding_response["geojson"] && geocoding_response["geojson"]["type"] != "Point"
      geocoding_response.delete("lat")
      geocoding_response.delete("lng")
    end
    if geocoding_response["address"]
      geocoding_response["address"]["city"] ||= begin
        geocoding_response["address"]["town"] || geocoding_response["address"]["village"]
      end
      geocoding_response["address"]["street"] ||= begin
        geocoding_response["address"]["road"] ||
          geocoding_response["address"]["footway"] ||
          geocoding_response["address"]["pedestrian"] ||
          geocoding_response["address"]["cycleway"] ||
          geocoding_response["address"]["bridleway"] ||
          geocoding_response["address"]["construction"] ||
          geocoding_response["address"]["path"] ||
          geocoding_response["address"].detect { |k, _| /address\d+/.match(k) }.to_a[1]
      end
    end
    geocoding_response.symbolize_keys
  end
end
