class Downloader
  MAX_RETRY = 2

  Error = Class.new(StandardError)
  NotFound = Class.new(StandardError)
  Forbidden = Class.new(StandardError)


  class Content
    attr_reader :visited_at

    def initialize(http_response)
      @http_response = http_response
      @visited_at = DateTime.now
    end

    def file
      @file ||= begin
        tempfile = Tempfile.open(encoding: "ASCII-8BIT")
        tempfile.write(@http_response.body)
        tempfile.rewind
        tempfile
      end
    end

    def extension
      @extension ||= begin
        mime_type = @http_response.headers["content-type"]
                                  .split(",")
                                  .first
                                  .split("\;")
                                  .first
        if mime_type == "application/octet-stream"
          filename_regexp = /filename=\"(.*)\"/.match(
            @http_response.headers["content-disposition"]
          )
          filename_regexp ||= /filename=(.*)/.match(
            @http_response.headers["content-disposition"]
          )
          File.extname(filename_regexp[1])
        else
          url_extension = File.extname(@http_response.env.url.path).downcase
          mime_type_extension = ".#{mime_type.split("/").last}"
          if Rack::Mime::MIME_TYPES[url_extension] == mime_type
            url_extension
          elsif Rack::Mime::MIME_TYPES[mime_type_extension] == mime_type
            mime_type_extension
          else
            Rack::Mime::MIME_TYPES.detect { |k, v| v == mime_type }.first
          end
        end
      end
    end
  end

  def initialize(logger)
    @logger = logger
    @http = Faraday.new do |faraday|
      faraday.use FaradayMiddleware::FollowRedirects
      faraday.response :logger, @logger
      faraday.adapter Faraday.default_adapter
    end
  end

  def get(url)
    perform_http(:get, url)
  end

  def head(url)
    perform_http(:head, url)
  end

  private

  def perform_http(verb, url, try = 0)
    response = @http.send(verb, url)
    raise Downloader::NotFound if response.status == 404
    raise Downloader::Forbidden if [401, 403].include?(response.status)
    raise Downloader::Error unless response.success?
    Content.new(response)
  rescue Faraday::Error => e
    return perform_http(verb, url, try + 1) if try < MAX_RETRY
    @logger.warn("#{verb} #{url} error #{e.class}: #{e.message}")
    raise Downloader::NotFound
  end
end
