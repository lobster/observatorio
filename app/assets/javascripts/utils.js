String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

function currentLocale() {
  return $('html').attr('lang');
}

function currentLanguage() {
  return currentLocale().split('-')[0];
}
