 function configureLinkModal(modal) {
  /*
  * Setup tooltips
  */
  $('.has-tooltip', modal).tooltip({trigger: 'hover'});
  /*
  * Setup label
  */
  $('select[name="link[predefined_label]"]', modal).change(function(event) {
    var select = $(event.target);
    var labelInput = $('input[name="link[label]"]', modal);
    var option = select.find(':selected');
    labelInput.val(option.val());
    if (option.html() && !option.val()) {
      select.next().show();
      labelInput.show();
      labelInput.focus();
    } else {
      select.next().hide();
      labelInput.hide();
    }
  });
  var contentInput = $('input[name="link[content_unencrypted_base64]"]', modal);
  var signatureInput = $('input[name="link[signature]"]', modal);
  var extensionInput = $('input[name="link[content_extension]"]', modal);
  var contentTag = $('.link-content', modal);
  /*
  * Setup ignore content
  */
  var ignoreContentInput = $('input[name="link[ignore_content]"]', modal);
  ignoreContentInput.change(function(event) {
    var checkbox = $(event.target);
    if (checkbox.is(':checked')) {
      contentInput.prop('disabled', true);
      signatureInput.prop('disabled', true);
      extensionInput.prop('disabled', true);
      contentTag.hide();
    } else {
      contentInput.prop('disabled', false);
      signatureInput.prop('disabled', false);
      extensionInput.prop('disabled', false);
      contentTag.show();
    }
  });
  /*
  * Render PDFs
  */
  var pdfContainer = $('.pdf-container', modal);
  var pdfNext = $('.pdf-next', modal);
  var pdfPrevious = $('.pdf-previous', modal);
  if (pdfContainer.length) {
    var pdfData = atob(contentInput.val());
    var loadingTask = renderPdf(pdfContainer[0], { data: pdfData });
    loadingTask.promise.then(function(pdf) {
      if (pdf.numPages > 1) {
        pdfNext.removeClass('disabled');
      }
    });
  }
  pdfNext.click(function(event) {
    event.preventDefault();
    var button = $(event.target);
    if (button.hasClass('disabled')) {
      return;
    }
    var lastPage = renderNextPage();
    if (lastPage) {
      pdfNext.addClass('disabled');
    }
    pdfPrevious.removeClass('disabled');
  });
  pdfPrevious.click(function(event) {
    event.preventDefault();
    var button = $(event.target);
    if (button.hasClass('disabled')) {
      return;
    }
    var firstPage = renderPreviousPage();
    if (firstPage) {
      pdfPrevious.addClass('disabled');
    }
    pdfNext.removeClass('disabled');
  });
  /*
  * Render images
  */
  var image = $('.content-image', modal);
  image.attr(
    'src', 'data:' + image.attr('data-mime') + ';base64,' + contentInput.val()
  );
  /*
  * Render videos
  */
  var video = $('.content-video', modal);
  $('source', video).attr(
    'src', 'data:' + video.attr('data-mime') + ';base64,' + contentInput.val()
  );
  /*
  * Play audios
  */
  var audio = $('.content-audio', modal);
  $('source', audio).attr(
    'src', 'data:' + audio.attr('data-mime') + ';base64,' + contentInput.val()
  );
}
