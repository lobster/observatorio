function initOrganizationMemberModal(modal) {
  // Configure departments
  var departmentsDiv = $('.refreshable-departments', modal);
  var organizationsSelect = $('select[name="organization_member[organization_id]"]', modal);
  var departmentsSelect = $('select[name="organization_member[department_id]"]');
  var loadingDepartments = $('.loading-departments', modal);
  var currentLocationInput = $('.current-location input', modal);
  organizationsSelect.change(function(event) {
    var select = $(event.target);
    var option = select.find(':selected');
    function refreshDepartments() {
      $.ajax({
        url: option.attr('data-departments-path'),
        dataType: 'json',
        success: function(json) {
          departmentsSelect.empty();
          departmentsSelect.append('<option value=""></option>');
          $.each(json, function(index, department) {
            departmentOption = $(
              '<option value ="' + department.id + '">' +
                department.name +
              '</option>'
            );
            if (department.location) {
              departmentOption.attr('data-location-id', department.location.id);
              departmentOption.attr('data-location-address', department.location.truncated_address);
            }
            departmentsSelect.append(departmentOption);
          });
          loadingDepartments.hide();
          departmentsSelect.show();
        }
      });
    }
    if (option.attr('data-departments-path')) {
      departmentsSelect.hide();
      loadingDepartments.show();
      setTimeout(refreshDepartments, 500);
    } else {
      departmentsSelect.empty();
    }

    var organizationLocation = $('.organization-location', modal);
    if (option.attr('data-headquarters-id') &&
          option.attr('data-headquarters-id') !== currentLocationInput.val()) {
      $('input', organizationLocation).val(option.attr('data-headquarters-id'));
      $('label', organizationLocation).html(option.attr('data-headquarters-address'));
      organizationLocation.show();
    } else {
      organizationLocation.hide();
    }
  });
  departmentsSelect.change(function(event) {
    select = $(event.target);
    var option = select.find(':selected');
    var departmentLocation = $('.department-location', modal);
    if (option.attr('data-location-id') &&
          option.attr('data-location-id') !== currentLocationInput.val()) {
      $('input', departmentLocation).val(option.attr('data-location-id'));
      $('label', departmentLocation).html(option.attr('data-location-address'));
      departmentLocation.show();
    } else {
      departmentLocation.hide();
    }
  });
}
