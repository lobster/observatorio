function initOrganizationOwnerForm(modal) {
  var ownerSelect = $('select[name="organization_owner[owner_id]"]', modal);
  ownerSelect.change(function() {
    var selected = ownerSelect.find(':selected');
    $('input[name="organization_owner[owner_type]"]', modal).val(selected.attr('data-type'));
  });
}
