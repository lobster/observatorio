function getForm(modal) {
  var contentDiv = $('.modal-form-content', modal);
  var loadingDiv = $('.loading', modal);
  contentDiv.hide();
  loadingDiv.show();
  $.ajax({
    url: contentDiv.attr('data-url'),
    success: function(html) {
      setForm(modal, html);
    }
  });
}

function setForm(modal, formHtml) {
  var contentDiv = $('.modal-form-content', modal);
  var loadingDiv = $('.loading', modal);
  contentDiv.empty();
  contentDiv.append(formHtml);
  $('input[name="refresh_element"]', contentDiv).each(function(index, element) {
    var input = $(element);
    function refreshSubsection(event) {
      var modal = $(event.target);
      var refreshable = $(input.val() + ' > .refreshable');
      refresh(refreshable);
      modal.off('hidden.bs.modal', refreshSubsection);
    }
    modal.on('hidden.bs.modal', refreshSubsection);
  });
  $('.timepicker', modal).timepicker({
    showButtonPanel: false
  });
  /*
  * Init custom file input plugin
  */
  bsCustomFileInput.init();
  var modalBody = $('.modal-body', modal);
  modalBody.animate({ scrollTop: 0 }, 'fast');
  loadingDiv.hide();
  contentDiv.show();
  var form = $('form', contentDiv);
  $('label.checkbox', form).click(function(event) {
    event.preventDefault();
    var label = $(event.target);
    var checkbox = $('input[type="checkbox"]', label);
    checkbox.prop('checked', !checkbox.is(':checked'));
    if (checkbox.is(':checked')){
      label.removeClass(label.attr('data-unchecked-class'));
      label.addClass(label.attr('data-checked-class'));
    } else {
      label.removeClass(label.attr('data-checked-class'));
      label.addClass(label.attr('data-unchecked-class'));
    }
  });
  form.submit(submitForm.bind(null, modal));
  configureFormMaps(modal);
  if (form.attr('data-init') && window[form.attr('data-init')]) {
    window[form.attr('data-init')](modal);
  }
  var autofocusInput = $('[data-autofocus]', contentDiv);
  autofocusInput.focus();
}

function submitForm(modal, event) {
  var contentDiv = $('.modal-form-content', modal);
  var loadingDiv = $('.loading', modal);
  var errorDiv = $('.modal-form-error', modal);
  contentDiv.hide();
  loadingDiv.show();
  var form = $(event.target);
  $.ajax({
    url: form.attr('action'),
    type: form.attr('method'),
    data: new FormData(form[0]),
    processData: false,
    contentType: false,
    success: function(html) {
      setForm(modal, html);
    },
    error: function() {
      loadingDiv.hide();
      errorDiv.show();
    }
  });
  return false;
}

function configureFormMaps(modal) {
  var form = $('form', modal);
  $('.location-group', form).each(function(index, element) {
    var locationGroup = $(element);
    var locationsPath = locationGroup.attr('data-locations-path');
    var locationInput = $('input[type="hidden"]', locationGroup);
    var mapContainer = $('.map-container', element);
    var modalBody = $('.modal-body', modal);
    mapContainer.hide();
    var locationRadio = $('input[type="radio"]', locationGroup);
    locationRadio.change(function(event) {
      var radio = $(event.target);
      if (radio.hasClass('location-search')) {
        locationInput.val(locationInput.attr('value'));
        var geomap = new GeoMap(mapContainer);
        geomap.disableScrollZoom();
        geomap.disableDoubleClickZoom();
        geomap.addSearch(form, locationInput);
        var searchInput = $('.mapboxgl-ctrl-geocoder input', locationGroup);
        searchInput.on('focus', function() {
          modalBody.animate({ scrollTop: mapContainer.position().top + modalBody.scrollTop() - 30 }, 'slow');
        });
        searchInput.focus();
      } else {
        locationInput.val(radio.val());
        mapContainer.hide();
        mapContainer.empty();
      }
    });
  });
}
