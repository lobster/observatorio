function refresh(refreshable) {
  var loadingDiv = refreshable.children('.loading');
  var contentDiv = refreshable.children('.content');
  if (refreshable.hasClass('refreshable-hide')) {
    contentDiv.hide();
  } else {
    contentDiv.css('visibility', 'hidden');
  }
  $('.action', contentDiv).hide();
  loadingDiv.show();
  if (!refreshable.attr('data-refresh')) {
    loadingDiv.hide();
    if (refreshable.hasClass('refreshable-hide')) {
      contentDiv.show();
    } else {
      contentDiv.css('visibility', 'visible');
    }
    $('.action', contentDiv).show();
    return;
  }
  $.ajax({
    url: refreshable.attr('data-refresh'),
    success: function(data) {
      var html = $(data);
      refreshable.replaceWith(html);
      initAll(html);
    },
    error: function(_, _, errorThrown) {
      if (errorThrown === "Not Found") {
        refreshable.remove();
      }
    }
  });
}
