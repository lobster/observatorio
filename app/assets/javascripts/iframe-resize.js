$(document).ready(function() {
  // Resize iFrames based on content
  $('.iframe-resize').iFrameResize({
    heightCalculationMethod: 'grow',
    resizedCallback: function() {
      $('.iframe-resize').removeClass('d-none');
    }
  });
});
