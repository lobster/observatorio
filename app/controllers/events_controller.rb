class EventsController < ResourcesController
  # GET /events/:id/description
  def description
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    render :description, layout: false
  end

  # GET /events/:id/description/edit
  def edit_description
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    render :form_description, layout: false
  end

  # PUT /events/:id/description
  def update_description
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @resource.assign_attributes(update_description_params)
    if @resource.save
      render :update_description, layout: false
    end
  end

  private

  def resources_class
    Event
  end

  def new_params
    {}
  end

  def create_params
    create_or_update_params
  end

  def update_params
    create_or_update_params
  end

  def update_description_params
    params.require(:event).permit(:description)
  end

  def create_or_update_params
    params.require(:event)
          .permit(:name,
                  :started_at,
                  :finished_at,
                  :approximate_dates,
                  :location_id,
                  :time_zone,
                  tag_ids: [])
  end

  def includes_associations
    @resources = @resources.includes(:tags)
  end
end
