class TagsController < ResourcesController
  def authorize!(action, resource)
    if resource == Tag
      subtype = params.require(:tag).require(:type).constantize
      authorize! action, subtype
    else
      super
    end
  end

  private

  def resources_class
    Tag
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:tag)
          .permit(:type)
  end

  def create_params
    params.require(:tag)
          .permit(:type, :name)
  end

  def update_params
    params.require(:tag)
          .permit(:name)
  end

  def versions_unsorted
    resource_versions
  end
end
