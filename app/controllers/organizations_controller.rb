class OrganizationsController < ResourcesController
  # GET /organizations/:id/description
  def description
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    render :description, layout: false
  end

  # GET /organizations/:id/description/edit
  def edit_description
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    render :form_description, layout: false
  end

  # PUT /organizations/:id/description
  def update_description
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @resource.assign_attributes(update_description_params)
    if @resource.save
      render :update_description, layout: false
    end
  end

  private

  def resources_class
    Organization
  end

  def sort_resources
    @resources = @resources.order_by_main_name
  end

  def new_params
    {}
  end

  def create_params
    create_or_update_params
  end

  def update_params
    create_or_update_params
  end

  def update_description_params
    params.require(:organization).permit(:description)
  end

  def create_or_update_params
    params.require(:organization)
          .permit(:name,
                  :cnpj,
                  :school,
                  :headquarters_id,
                  :date_of_foundation,
                  :date_of_extinction,
                  :approximate_dates,
                  tag_ids: [])
  end

  def versions_unsorted
    versions_merged_with_names_unsorted
  end

  def includes_associations
    @resources = @resources
                   .includes(:tags,
                             :picture,
                             :main_name)
  end
end
