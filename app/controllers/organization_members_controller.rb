class OrganizationMembersController < ResourcesController
  layout false

  # GET /organization_members
  def index
    super
    @resources = @resources
                   .includes(:member,
                             :location,
                             :sources,
                             :attachments,
                             organization: [:names])
                   .order("names.value")
  end

  private

  def authorize!(action, resource)
    if action == :list && resource == OrganizationMember
      member_id = params.require(:organization_member)[:member_id]
      organization_id = params.require(:organization_member)[:organization_id]
      if member_id.present?
        member = Person.find(member_id)
        authorize! :read, member
      elsif organization_id.present?
        organization = Organization.find(organization_id)
        authorize! :read, organization
      else
        department_id = params.require(:organization_member).require(:department_id)
        department = OrganizationDepartment.find(department_id)
        authorize! :read, department
      end
    else
      super
    end
  end

  def resources_per_page
    nil
  end

  def resources_class
    OrganizationMember
  end

  def index_params
    index_or_new_params
  end

  def new_params
    index_or_new_params
  end

  def index_or_new_params
    params.require(:organization_member)
          .permit(:member_id,
                  :organization_id,
                  :department_id)
          .tap do |hash|
      if hash.key?(:department_id) && hash[:department_id].empty?
        hash[:department_id] = nil
      end
    end
  end

  def create_params
    params.require(:organization_member)
          .permit(:member_id,
                  :organization_id,
                  :position_id,
                  :department_id,
                  :from_date,
                  :to_date,
                  :approximate_dates,
                  :location_id)
  end

  def update_params
    params.require(:organization_member)
          .permit(:position_id,
                  :department_id,
                  :from_date,
                  :to_date,
                  :approximate_dates,
                  :location_id)
  end
end
