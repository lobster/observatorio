class OrganizationPositionsController < ResourcesController
  private

  def resources_class
    OrganizationPosition
  end

  def new_params
    {}
  end

  def create_params
    create_or_update_params
  end

  def update_params
    create_or_update_params
  end

  def create_or_update_params
    params.require(:organization_position)
          .permit(:name)
  end

  def versions_unsorted
    resource_versions
  end
end
