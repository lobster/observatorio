class HomeController < ApplicationController
  skip_before_action :authenticate_user!
  skip_authorization_check

  # GET /
  def index
    return redirect_to(people_path) if user_signed_in?
    redirect_to(new_user_session_path)
  end
end
