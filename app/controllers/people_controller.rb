class PeopleController < ResourcesController
  # GET /people/:id/biography
  def biography
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    render :biography, layout: false
  end

  # GET /people/:id/biography/edit
  def edit_biography
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    render :form_biography, layout: false
  end

  # PUT /people/:id/biography
  def update_biography
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @resource.assign_attributes(update_biography_params)
    if @resource.save
      render :update_biography, layout: false
    end
  end

  private

  def resources_class
    Person
  end

  def sort_resources
    @resources = @resources.order_by_main_name
  end

  def new_params
    {}
  end

  def create_params
    create_or_update_params
  end

  def update_params
    create_or_update_params
  end

  def update_biography_params
    params.require(:person).permit(:biography)
  end

  def create_or_update_params
    params.require(:person)
          .permit(:name,
                  :date_of_birth,
                  :date_of_death,
                  :birth_location_id,
                  :death_location_id,
                  :approximate_dates,
                  nationalities: [],
                  tag_ids: [])
  end

  def versions_unsorted
    versions_merged_with_names_unsorted
  end

  def associations_versions
    super
      .where
      .not("item_subtype = :relationship_klass AND \
            foreign_key_name = :with_id_name",
           relationship_klass: Relationship.name, with_id_name: "with_id")
  end

  def includes_associations
    @resources = @resources
                   .includes(:tags,
                             :picture,
                             :main_name)
  end
end
