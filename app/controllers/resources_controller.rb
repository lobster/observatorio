class ResourcesController < ApplicationController
  # GET /[collection]
  def index
    authorize! :list, resources_class
    @resources = resources_class.where(index_params)
    if @resources.respond_to?(:search) && params[:search].present?
      @resources = @resources.search(params[:search])
      @search = params[:search]
    end
    if resources_per_page.present?
      count = @resources.count
      @page = [params[:page].present? ? params[:page].to_i : 1, 1].max
      offset = (@page - 1) * resources_per_page
      @resources = @resources.offset(offset)
      @last_page = offset + resources_per_page >= count
      @resources = @resources.limit(resources_per_page)
    end
    sort_resources
    includes_associations
  end

  # GET /[collection]/new
  def new
    @resource = resources_class.new(new_params)
    authorize! :create, @resource
    render :form, layout: false
  end

  # POST /[collection]
  def create
    @resource = resources_class.new(create_params)
    authorize! :create, @resource
    if @resource.save
      render :create, layout: false
    else
      render :form, layout: false
    end
  end

  # GET /[collection]/:id
  def show
    @resource = resources_class.find(params[:id])
    authorize! :read, @resource
  end

  # GET /[collection]/:id/edit
  def edit
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    render :form, layout: false
  end

  # PUT /[collection]/:id
  def update
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @resource.assign_attributes(update_params)
    if @resource.save
      render :update, layout: false
    else
      render :form, layout: false
    end
  end

  # DELETE /[collection]/:id
  def destroy
    @resource = resources_class.find(params.require(:id))
    authorize! :delete, @resource
    if @resource.destroy
      render :destroy, layout: false
    else
      @error_message = @resource.errors[:base].first
      render :error, layout: false
    end
  end

  # GET /[collection]/:id/history
  def history
    @resource = resources_class.find(params.require(:id))
    authorize! :read, @resource
    @versions = versions_unsorted.sort do |a, b|
      b.created_at <=> a.created_at
    end
    render :history, layout: false
  end

  private

  def resources_per_page
    8
  end

  def index_params
    "1 = 1"
  end

  def versions_unsorted
    resource_versions + associations_versions + associations_sources_versions
  end

  def versions_merged_with_names_unsorted
    all_versions = resource_versions +
                     associations_versions +
                     associations_sources_versions
    versions_grouped = all_versions.group_by(&:transaction_id)
    versions_grouped.each do |transaction_id, versions|
      person_version = versions.detect do |version|
        version.item_subtype == @resource.class.name
      end
      main_name_version = versions.detect do |version|
        version.item_subtype == Name.name && version.item_state.main?
      end
      if main_name_version.present?
        if person_version.present?
          person_version.changeset["name"] = main_name_version.changeset["value"]
          all_versions.delete(main_name_version)
        else
          main_name_version.item = main_name_version.item.nameable
          main_name_version.changeset["name"] = main_name_version.changeset["value"]
        end
      end
    end
    all_versions
  end

  def resource_versions
    @resource_versions ||= begin
      PaperTrail::Version
        .includes(:user)
        .where("whodunnit IS NOT NULL")
        .where(item_type: resources_class.name)
        .where(item_id: @resource.id)
    end
  end

  def associations_versions
    @associations_versions ||= begin
      PaperTrail::Version
        .includes(:user)
        .joins(:version_associations)
        .where("whodunnit IS NOT NULL")
        .where("version_associations.foreign_type = ?", resources_class)
        .where("version_associations.foreign_key_id = ?", @resource.id)
    end
  end

  def associations_sources_versions
    @associations_sources_versions ||= begin
      return [] if associations_versions.count.zero?
      query_str = ""
      query_params = {}
      associations_versions.each_with_index do |version, index|
        query_str << " OR " if query_str.present?
        query_str << "(version_associations.foreign_type = :foreign_type#{index} AND \
                       version_associations.foreign_key_id = :foreign_key_id#{index})"
        query_params[:"foreign_type#{index}"] = version.item_type
        query_params[:"foreign_key_id#{index}"] = version.item_id
      end
      PaperTrail::Version
        .includes(:user)
        .joins(:version_associations)
        .where("whodunnit IS NOT NULL")
        .where("item_type = :link OR item_type = :attachment",
               link: Link, attachment: Attachment)
        .where(query_str, query_params)
    end
  end

  def sort_resources
  end

  def includes_associations
  end
end
