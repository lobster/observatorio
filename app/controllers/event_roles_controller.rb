class EventRolesController < ResourcesController
  private

  def resources_class
    EventRole
  end

  def new_params
    {}
  end

  def create_params
    create_or_update_params
  end

  def update_params
    create_or_update_params
  end

  def create_or_update_params
    params.require(:event_role)
          .permit(:name)
  end

  def versions_unsorted
    resource_versions
  end
end
