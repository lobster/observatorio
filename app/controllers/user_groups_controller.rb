class UserGroupsController < ResourcesController
  # POST /user_groups/:id/users
  def add_user
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @user = User.find(params.require(:user_id))
    if @resource.users.include?(@user)
      head 304
    else
      @resource.users.append(@user)
      @resource.save!
      render :add_user, layout: false
    end
  end

  # DELETE /user_groups/:id/users/:user_id
  def remove_user
    @resource = resources_class.find(params.require(:id))
    authorize! :update, @resource
    @user = User.find(params.require(:user_id))
    if @resource.users.include?(@user)
      @resource.users.delete(@user)
      @resource.save!
      render :remove_user, layout: false
    else
      head 304
    end
  end

  private

  def resources_class
    UserGroup
  end

  def resources_per_page
    nil
  end
end
