module OrganizationOwnersHelper
  def organization_owners_options
    names_sorted = Name.where(main: true)
                       .where(nameable_type: [Person.name, Organization.name])
                       .where.not(
                         "nameable_id = :organization_id AND
                          nameable_type = :organization_type",
                         organization_type: Organization.name,
                         organization_id: @resource.organization_id
                       )
                       .order(:value)
    names_sorted.map do |name|
      [name.value, name.nameable_id, :"data-type" => name.nameable_type]
    end
  end
end
