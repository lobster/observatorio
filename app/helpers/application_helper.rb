module ApplicationHelper
  def people_options
    names = Name.where(main: true)
                .where(nameable_type: Person.name)
    if @resource.is_a?(Person)
      names = names.where("nameable_id != ?", @resource.id)
    end
    names.order(:value).map do |name|
      [name.value, name.nameable_id]
    end
  end

  def organizations_options
    Organization
      .includes(:headquarters, :main_name)
      .where("organizations.id != ?", @resource.id || SecureRandom.uuid)
      .order_by_main_name
      .map do |organization|
      [display_name(organization), organization.id, {
        :"data-departments-path" => organization_departments_path({
          organization_department: { organization_id: organization.id }
        }),
        :"data-headquarters-id" => organization.headquarters_id,
        :"data-headquarters-address" => truncated_address(organization.headquarters)
      }]
    end
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  def display_user(user)
    user.present? ? user.email : User.human_attribute_name(:deleted)
  end

  def display_time(value, format: :long, time_zone: nil)
    return nil if value.nil?
    l(time_zone.nil? ? value : value.in_time_zone(time_zone), format: format)
  end

  def display_name(object)
    return nil if object.nil?
    return object.display_name if object.respond_to?(:display_name)
    return object.full_name if object.respond_to?(:full_name)
    return object.name if object.respond_to?(:name)
    return object.label if object.respond_to?(:label)
    return object.email if object.is_a?(User)
    return display_address(object.address) if object.is_a?(Location)
    object.to_s
  end

  def truncated_address(location)
    return nil if location.nil?
    truncate(display_name(location), length: 70)
  end

  def display_resource(object)
    return nil if object.nil?
    if object.persisted?
      if can? :read, object
        link_to(display_name(object), url_for(object))
      else
        display_name(object)
      end
    else
      tag.span(display_name(object), class: "text-muted")
    end
  end

  def display_boolean(value)
    return nil if value.nil?
    t("observatorio.#{value}")
  end

  def display_percentage(value)
    return nil if value.nil?
    "#{(value * 100).to_i}"
  end

  private

  def display_address(address)
    displayed_components = []
    if address["house_number"].present?
      displayed_components << address["house_number"]
    end
    if address["street"].present?
      displayed_components << address["street"]
    else
      if address["city_district"] && address["city_district"] != address["city"]
        displayed_components << address["city_district"]
      elsif address["suburb"]
        displayed_components << address["suburb"]
      end
    end
    if address["city"].present?
      displayed_components << address["city"]
    end
    if address["city"].nil? && address["county"].present?
      displayed_components << address["county"]
    end
    if address["state_district"] && address["city"].nil?
      displayed_components << address["state_district"]
    end
    if address["state"].present?
      displayed_components << address["state"]
    end
    if address["country"]
      displayed_components << address["country"]
    end
    displayed_components.join(", ").html_safe
  end

  def time_zone_options
    ActiveSupport::TimeZone.all.map { |tz| [format_timezone(tz), tz.tzinfo.name] }
  end

  private

  def format_timezone(time_zone)
    tz = Time.find_zone!(time_zone.tzinfo.name)
    formatted_utc_offset = begin
      if tz.utc_offset.zero?
        "(UTC/GMT)"
      elsif tz.utc_offset.positive?
        "(UTC/GMT + #{tz.utc_offset / 1.hour})"
      else
        "(UTC/GMT - #{tz.utc_offset.abs / 1.hour})"
      end
    end
    "#{tz.tzinfo.name} #{formatted_utc_offset}"
  end

  def refresh_element
    if @resource.is_a?(Link) &&
       (@resource.linkable.is_a?(Relationship) ||
        @resource.linkable.is_a?(Education) ||
        @resource.linkable.is_a?(OrganizationMember) ||
        @resource.linkable.is_a?(OrganizationOwner) ||
        @resource.linkable.is_a?(EventParticipant))
      "##{@resource.linkable_type.underscore}-#{@resource.linkable_id}"
    elsif @resource.is_a?(Attachment) &&
          (@resource.attachable.is_a?(Relationship) ||
           @resource.attachable.is_a?(Education) ||
           @resource.attachable.is_a?(OrganizationMember) ||
           @resource.attachable.is_a?(OrganizationOwner) ||
           @resource.attachable.is_a?(EventParticipant))
      "##{@resource.attachable_type.underscore}-#{@resource.attachable_id}"
    elsif @resource.is_a?(OrganizationDepartment) && @resource.parent_id?
      "#children-#{@resource.parent_id}"
    else
      "##{@resource.class.name.pluralize.underscore}-subsection"
    end
  end

  def display_date(datetime)
    return nil if datetime.nil?
    datetime.to_date
  end

  def display_time_in_day(datetime)
    return nil if datetime.nil?
    datetime.strftime("%H:%M")
  end

  def origin_params
    return nil unless params.key?(:origin)
    params.require(:origin)
          .permit(:id, :controller)
          .merge(action: "show")
  end
end
