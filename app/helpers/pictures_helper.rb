module PicturesHelper
  def picture_placeholder_url
    case params.require(:picture)[:picturable_type]
    when "Person"
      image_path("open-iconic/person.svg")
    when "Organization"
      image_path("open-iconic/home.svg")
    end
  end

  def picture_display_url(picture)
    case picture.picturable_type
    when "Person"
      picture.face.url
    when "Organization"
      picture.show.url
    end
  end
end
