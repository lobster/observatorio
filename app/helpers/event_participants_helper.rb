module EventParticipantsHelper
  def participants_options
    names_sorted = Name.where(main: true)
                       .where(nameable_type: [Person.name, Organization.name])
                       .order(:value)
    names_sorted.map do |name|
      [name.value, name.nameable_id, :"data-type" => name.nameable_type]
    end
  end

  def roles_options
    @_roles_options ||= begin
      EventRole.all.map { |o| [display_name(o), o.id] }.sort_by(&:first)
    end
  end

  def events_options
    Event.all.map do |o|
      [display_name(o), o.id, {
        :"data-location-id" => o.location_id,
        :"data-location-address" => truncated_address(o.location),
        :"data-started-at" => o.started_at && o.started_at.strftime("%Y-%m-%d %H:%M"),
        :"data-finished-at" => o.finished_at && o.finished_at.strftime("%Y-%m-%d %H:%M"),
        :"data-time-zone" => o.time_zone
      }]
    end
  end
end
