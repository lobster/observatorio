object @resource

attribute :id
attribute :osm_id
attribute :osm_type
attribute :lat
attribute :lng
attribute :boundingbox
attribute :address
attribute :geojson

node(:formatted_address) do |location|
  display_name(location)
end
node(:truncated_address) do |location|
  truncated_address(location)
end
