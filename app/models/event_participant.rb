class EventParticipant < ApplicationRecord
  self.table_name = "events_participants"

  has_paper_trail
  has_location
  has_immutable_attributes :event_id, :participant_id, :participant_type
  has_date_boundaries from_date: :from_datetime, to_date: :to_datetime
  has_time_zone allow_nil: true
  has_links :sources
  has_attachments

  default_value :from_datetime, lambda { |o| o.event && o.event.started_at }
  default_value :to_datetime, lambda { |o| o.event && o.event.finished_at }
  default_value :approximate_dates, lambda { |o| o.event && o.event.approximate_dates }
  default_value :time_zone, lambda { |o| o.event && o.event.time_zone }

  belongs_to :event
  belongs_to :role, {
    class_name: "EventRole",
    inverse_of: :event_participants
  }
  belongs_to :participant, {
    inverse_of: :events_participant,
    polymorphic: true
  }

  validates_presence_of :event
  validates_presence_of :participant

  validate              :validate_duplicate

  def from_datetime_in_time_zone
    return nil if from_datetime.nil? || time_zone.nil?
    from_datetime.in_time_zone(time_zone)
  end

  def to_datetime_in_time_zone
    return nil if to_datetime.nil? || time_zone.nil?
    to_datetime.in_time_zone(time_zone)
  end

  private

  def validate_duplicate
    return unless event.present? && participant.present?
    return if self.class
                  .where(":id IS NULL OR id != :id", id: id)
                  .where(event: event)
                  .where(participant: participant)
                  .where(role: role)
                  .where(location: location)
                  .where("from_datetime <= :to_datetime OR " \
                         ":to_datetime IS NULL OR " \
                         "from_datetime IS NULL",
                         to_datetime: to_datetime)
                  .where("to_datetime >= :from_datetime OR " \
                         ":from_datetime IS NULL OR " \
                         "to_datetime IS NULL",
                         from_datetime: from_datetime)
                  .empty?
    errors.add(:event_id, I18n.t("errors.messages.taken"))
    errors.add(:participant_id, I18n.t("errors.messages.taken"))
  end
end
