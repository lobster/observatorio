class Event < ApplicationRecord
  default_scope { order(:name) }

  scope :search, lambda { |query|
    where("UNACCENT(name || name) ilike UNACCENT(?)", "%#{query.split.join("%")}%")
  }


  has_date_boundaries from_date: :started_at, to_date: :finished_at
  has_paper_trail
  has_time_zone allow_nil: true
  has_links
  has_attachments
  has_notes
  has_location

  has_many :participants, {
    class_name: "EventParticipant",
    inverse_of: :event,
    foreign_key: "event_id",
    dependent: :destroy
  }
  has_many :tagged_events, {
    class_name: "TaggedEvent",
    inverse_of: :event,
    foreign_key: "event_id"
  }
  has_many :tags, through: :tagged_events, dependent: :destroy

  validates_presence_of :name
  validates_presence_of :time_zone, if: lambda { |o| o.started_at.present? || o.finished_at.present? }
  validates_uniqueness_of :name, case_sensitive: false

  def started_at_in_time_zone
    started_at && started_at.in_time_zone(time_zone)
  end

  def finished_at_in_time_zone
    finished_at && finished_at.in_time_zone(time_zone)
  end
end
