class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  include DateBoundaries
  include DefaultValue
  include HasAttachments
  include HasFile
  include HasImmutableAttributes
  include HasLinks
  include HasLocation
  include HasNameAndDisplayPriority
  include HasNames
  include HasNotes
  include HasPicture
  include HasTimeZone
  include Strip
end
