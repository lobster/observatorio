class Ability
  include CanCan::Ability

  def initialize(user)
    # Anonymous users have no access
    return unless user.present?
    # Permissions set in database
    user.groups.each do |group|
      group.permissions.each do |klass, config|
        config.each do |action|
          can action.to_sym, klass.constantize
        end
      end
    end
    # Name
    can :read, Name do |name|
      can? :read, name.nameable
    end
    can :update, Name do |name|
      can? :update, name.nameable
    end
    can [:create, :delete], Name do |name|
      can?(:update, name.nameable) && !name.main?
    end
    # Picture
    can :read, Picture do |picture|
      can? :read, picture.picturable
    end
    can [:create, :update, :delete], Picture do |picture|
      can? :update, picture.picturable
    end
    # Attachment
    can :read, Attachment do |attachment|
      can? :read, attachment.attachable
    end
    can [:create, :update, :delete], Attachment do |attachment|
      can? :update, attachment.attachable
    end
    # Link
    can :read, Link do |link|
      can? :read, link.linkable
    end
    can [:create, :update, :delete], Link do |link|
      can? :update, link.linkable
    end
    # Relationship
    can :read, Relationship do |relationship|
      (relationship.owner.nil? || can?(:read, relationship.owner)) &&
        (relationship.with.nil? || can?(:read, relationship.with))
    end
    can [:create, :update, :delete], Relationship do |relationship|
      (relationship.owner.nil? || can?(:update, relationship.owner)) &&
        (relationship.with.nil? || can?(:update, relationship.with))
    end
    # Education
    can :read, Education do |education|
      (education.school.nil? || can?(:read, education.school)) &&
        (education.student.nil? || can?(:read, education.student))
    end
    can [:create, :update, :delete], Education do |education|
      (education.school.nil? || can?(:update, education.school)) &&
        (education.student.nil? || can?(:update, education.student))
    end
    # OrganizationDepartment
    can :read, OrganizationDepartment do |department|
      department.organization.nil? || can?(:read, department.organization)
    end
    can [:create, :update, :delete], OrganizationDepartment do |department|
      (department.organization.nil? || can?(:update, department.organization)) &&
        (department.parent.nil? || can?(:update, department.parent))
    end
    # OrganizationMember
    can :read, OrganizationMember do |organization_member|
      (organization_member.member.nil? || can?(:read, organization_member.member)) &&
        (organization_member.organization.nil? || can?(:read, organization_member.organization))
    end
    can [:create, :update, :delete], OrganizationMember do |organization_member|
      (organization_member.member.nil? || can?(:update, organization_member.member)) &&
        (organization_member.organization.nil? || can?(:update, organization_member.organization))
    end
    # OrganizationOwner
    can :read, OrganizationOwner do |organization_owner|
      (organization_owner.owner.nil? || can?(:read, organization_owner.owner)) &&
        (organization_owner.organization.nil? || can?(:read, organization_owner.organization))
    end
    can [:create, :update, :delete], OrganizationOwner do |organization_owner|
      (organization_owner.owner.nil? || can?(:update, organization_owner.owner)) &&
        (organization_owner.organization.nil? || can?(:update, organization_owner.organization))
    end
    # EventParticipant
    can :read, EventParticipant do |event_participant|
      (event_participant.participant.nil? || can?(:read, event_participant.participant)) &&
        (event_participant.event.nil? || can?(:read, event_participant.event))
    end
    can [:create, :update, :delete], EventParticipant do |event_participant|
      (event_participant.participant.nil? || can?(:update, event_participant.participant)) &&
        (event_participant.event.nil? || can?(:update, event_participant.event))
    end
    # Note
    can [:read, :create], Note do |note|
      can?(:read, note.subject)
    end
    can :delete, Note, author_id: user.id
  end
end
