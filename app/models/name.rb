class Name < ApplicationRecord
  has_paper_trail

  default_value :main, false

  strip :value

  belongs_to :nameable, polymorphic: true

  validates_presence_of :value
  validates_presence_of :nameable
  validates_uniqueness_of :value, {
    if: :main?,
    case_sensitive: false,
    scope: :nameable_type
  }
  validates_uniqueness_of :main, {
    if: :main?,
    scope: [:nameable_id, :nameable_type]
  }
end
