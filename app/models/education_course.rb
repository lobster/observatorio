class EducationCourse < ApplicationRecord
  has_name_and_display_priority
  has_paper_trail

  has_many :educations, -> { includes(:student, :school,
                                      :degree, :course, :sources)
                               .order("people.first_name, people.last_name") }, {
    inverse_of: :course,
    foreign_key: "course_id",
    dependent: :restrict_with_error
  }

  validates_uniqueness_of :name, case_sensitive: false
end
