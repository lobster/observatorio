class Import < ApplicationRecord
  default_scope { order(created_at: :desc) }

  has_file

  default_value :status, "pending"
  default_value :logs, []

  belongs_to :user

  validates_presence_of  :user
  validates_inclusion_of :status, in: %w(pending processing success error)

  validate :validate_ods_file
  validate :validate_headers

  def display_name
    "#{self.class.model_name.human} - #{I18n.l(created_at, format: :long)}"
  end

  def finished?
    %w(success error).include?(status)
  end

  def log_info(message, line_number)
    return if message.nil?
    new_log = { severity: "info", line_number: line_number, message: message }
    update_attributes!(logs: logs + [new_log])
  end

  def log_error(message, line_number = nil)
    return if message.nil?
    new_log = { severity: "error", line_number: line_number, message: message }
    update_attributes!(logs: logs + [new_log])
  end

  def each_line_with_number
    (2..sheet.count).each do |line_number|
      current_row = sheet.row(line_number)
      line = columns.each_with_index
                    .each_with_object({}) do |(column, index), hash|
        value = current_row[index]
        value.strip! if value.is_a?(String)
        if hash.key?(column)
          hash[column] = [hash[column]] unless hash[column].is_a?(Array)
          hash[column] << value
        else
          hash[column] = value
        end
      end
      yield(line, line_number)
    end
  end

  private

  def validate_ods_file
    return unless ods.nil?
    errors.add(:file, I18n.t("errors.invalid_format"))
  end

  def validate_headers
    return if ods.nil?
    missing_columns = mandatory_columns - columns
    return if missing_columns.empty?
    errors.add(:file, I18n.t(
      "errors.import.missing_columns",
      missing_columns: missing_columns.join(", ")
    ))
  end

  def columns
    @columns ||= begin
      sheet.row(1)
           .select(&:present?)
           .map(&:downcase)
           .map(&:to_sym)
    end
  end

  def sheet
    ods.sheet(0)
  end

  def ods
    return nil unless file_extension == ".ods"
    decrypted = file_decrypted
    @ods ||= Roo::OpenOffice.new(decrypted.path) rescue nil
  end

  def mandatory_columns
    raise NotImplementedError
  end
end
