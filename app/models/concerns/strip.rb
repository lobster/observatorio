module Strip
  extend ActiveSupport::Concern

  class_methods do
    def strip(*attrs)
      attrs.each do |attribute|
        before_validation :"_strip_#{attribute}"

        define_method :"_strip_#{attribute}" do
          self[attribute] = self[attribute].strip unless self[attribute].nil?
        end
      end
    end
  end
end
