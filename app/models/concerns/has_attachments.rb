module HasAttachments
  extend ActiveSupport::Concern

  class_methods do
    def has_attachments
      has_many :attachments, {
        as: :attachable,
        dependent: :destroy
      }
    end
  end
end
