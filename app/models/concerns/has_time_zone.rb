module HasTimeZone
  extend ActiveSupport::Concern

  class_methods do
    def has_time_zone(attribute: :time_zone, allow_nil: false)
      default_value attribute, proc { Time.zone.tzinfo.name }

      # Time zones are stored in the format of the keys of ActiveSupport::TimeZone::MAPPINGS
      # because this data is only used in Rails context so far
      validates attribute, inclusion: { in: ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name), allow_nil: allow_nil }
    end
  end
end
