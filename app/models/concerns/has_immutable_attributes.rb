module HasImmutableAttributes
  extend ActiveSupport::Concern

  class_methods do
    def has_immutable_attributes(*attrs)
      attrs.each do |attribute|
        validate :"#{attribute}_unchaged", on: :update

        define_method :"#{attribute}_unchaged" do
          return unless send(:"#{attribute}_changed?")
          errors.add(:url, I18n.t("errors.cannot_change"))
        end
      end
    end
  end
end
