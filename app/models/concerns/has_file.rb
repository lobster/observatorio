module HasFile
  extend ActiveSupport::Concern

  class_methods do
    def has_file(attribute = :file, mandatory: true)
      mount_uploader attribute, FogUploader

      if mandatory
        validates_presence_of attribute
        validates_presence_of :"#{attribute}_size"
        validates_presence_of :"#{attribute}_encryption_iv_base64"
      end

      strip :"#{attribute}_encryption_iv_base64"

      before_validation :"downcase_#{attribute}_extension"

      define_method :"downcase_#{attribute}_extension" do
        return unless send(:"#{attribute}_extension").present?
        assign_attributes(
          :"#{attribute}_extension" => send(:"#{attribute}_extension").downcase
        )
      end

      define_method :"#{attribute}_unencrypted=" do |unencrypted|
        return if unencrypted.nil?
        encryption_iv = CIPHER.random_iv
        assign_attributes(
          :"#{attribute}_encryption_iv_base64" => Base64.encode64(encryption_iv).strip
        )
        assign_attributes(
          :"#{attribute}_size" => unencrypted.size
        )
        assign_attributes(
          :"#{attribute}_md5_hex" => Digest::MD5.hexdigest(unencrypted.read)
        )
        unencrypted.rewind
        assign_attributes(
          :"#{attribute}_sha2_hex" => Digest::SHA2.hexdigest(unencrypted.read)
        )
        unencrypted.rewind
        encrypted_data = CIPHER.encrypt(unencrypted.read, encryption_iv)
        tempfile = Tempfile.open(encoding: "ASCII-8BIT")
        tempfile.write(encrypted_data)
        tempfile.rewind
        assign_attributes(attribute => tempfile)
      end

      define_method :"#{attribute}_unencrypted_base64=" do |base64|
        tempfile = Tempfile.open(encoding: "ASCII-8BIT")
        tempfile.write(Base64.decode64(base64))
        tempfile.rewind
        send(:"#{attribute}_unencrypted=", tempfile)
      end

      define_method :"#{attribute}_decrypted" do
        return nil unless send(attribute).present?
        encryption_iv = Base64.decode64(send(:"#{attribute}_encryption_iv_base64"))
        decrypted_data = CIPHER.decrypt(send(attribute).read, encryption_iv)
        tempfile = Tempfile.open(
          ["", send(:"#{attribute}_extension")],
          encoding: "ASCII-8BIT"
        )
        tempfile.write(decrypted_data)
        tempfile.rewind
        tempfile
      end

      define_method :"#{attribute}_decrypted_base64" do
        return nil unless send(attribute).present?
        Base64.encode64(send(:"#{attribute}_decrypted").read).strip
      end

      define_method :"#{attribute}_mime_type" do
        extension = send(:"#{attribute}_extension")
        Rack::Mime::MIME_TYPES[extension]
      end

      define_method :"#{attribute}_text?" do
        send(:"#{attribute}_mime_type") == "text/plain"
      end

      define_method :"#{attribute}_html?" do
        send(:"#{attribute}_mime_type") == "text/html"
      end

      define_method :"#{attribute}_pdf?" do
        send(:"#{attribute}_mime_type") == "application/pdf"
      end

      define_method :"#{attribute}_image?" do
        mime_type = send(:"#{attribute}_mime_type")
        mime_type.present? && mime_type.starts_with?("image/")
      end

      define_method :"#{attribute}_video?" do
        mime_type = send(:"#{attribute}_mime_type")
        %w(video/mp4 video/webm application/ogg).include?(mime_type)
      end

      define_method :"#{attribute}_audio?" do
        mime_type = send(:"#{attribute}_mime_type")
        %w(audio/mpeg audio/x-wav application/ogg).include?(mime_type)
      end
    end
  end
end
