class Note < ApplicationRecord
  default_scope { order(created_at: :desc) }

  belongs_to :author, class_name: "User"
  belongs_to :subject, polymorphic: true

  validates_presence_of :author
  validates_presence_of :subject
  validates_presence_of :body
end
