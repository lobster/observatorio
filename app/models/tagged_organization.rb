class TaggedOrganization < ApplicationRecord
  self.table_name = "organizations_tags"

  has_paper_trail

  belongs_to :organization
  belongs_to :tag, class_name: "OrganizationTag"
end
