class PersonTag < Tag
  has_many :tagged_people, foreign_key: "tag_id"
  has_many :people, through: :tagged_people

  validates_uniqueness_of :name, case_sensitive: false
end
