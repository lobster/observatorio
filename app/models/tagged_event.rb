class TaggedEvent < ApplicationRecord
  self.table_name = "events_tags"

  has_paper_trail

  belongs_to :event
  belongs_to :tag, class_name: "EventTag"
end
