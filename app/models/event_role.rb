class EventRole < ApplicationRecord
  has_name_and_display_priority
  has_paper_trail

  has_many :event_participants, -> { includes(:event, :role, :sources) }, {
    inverse_of: :role,
    foreign_key: "role_id",
    dependent: :restrict_with_error
  }

  validates_uniqueness_of :name, case_sensitive: false
end
