class Tag < ApplicationRecord
  has_name_and_display_priority
  has_paper_trail
  has_immutable_attributes :type

  validate :valid_type

  private

  def valid_type
    return if Tag.subclasses.map(&:name).include?(type.to_s)
    errors.add(:type, I18n.t("errors.messages.inclusion"))
  end
end
