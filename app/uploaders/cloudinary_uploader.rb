class CloudinaryUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  process convert: "jpg"

  version :show do
    process resize_to_fill: [256, 256, :center]
  end

  version :thumb do
    process resize_to_fill: [80, 80, :center]
  end

  version :face do
    process resize_to_fill: [256, 256, :face]
  end

  version :face_thumb do
    process resize_to_fill: [80, 80, :face]
  end

  def public_id
    "observatorio/#{model.class.to_s.underscore}/#{mounted_as}/#{picture_id}"
  end

  private

  def picture_id
    SecureRandom.uuid
  end
end
