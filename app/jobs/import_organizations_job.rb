class ImportOrganizationsJob < ImportJob
  class CreateOrUpdateOrganization
    def initialize(name, attributes)
      @name = name
      @attributes = attributes
    end

    def run
      organization = Organization.find_by_main_name(@name)
      organization.present? ? update(organization) : create
    end

    private

    def create
      organization_ambiguous = Organization.search(@name)
                                           .where("names.value != :name",
                                                   name: @name)
      unless organization_ambiguous.empty?
        raise ImportError, I18n.t("errors.import.ambiguous_name",
                                  existing_name: organization_ambiguous.first.name,
                                  ambiguous_name: @name)
      end
      organization = Organization.create!(@attributes.merge(name: @name))
      I18n.t("observatorio.import.logs.organization_created",
             organization: "#{organization.name} (#{organization.id})")
    rescue ActiveRecord::RecordInvalid => e
      raise ImportError, I18n.t("errors.import.create_error",
                                 model: Organization.model_name.human.downcase,
                                 message: e.message)
    end

    def update(organization)
      if @attributes.key?(:tags)
        @attributes[:tags] += organization.tags
      end
      organization.assign_attributes(@attributes)
      return nil unless organization.changed?
      changes = organization.changes
                            .keys
                            .map(&:to_sym)
                            .map { |field| Organization.human_attribute_name(field) }
      organization.save!
      I18n.t("observatorio.import.logs.organization_updated",
             organization: "#{organization.name} (#{organization.id})",
             changes: changes.join(", "))
    rescue ActiveRecord::RecordInvalid => e
      raise ImportError, I18n.t("errors.import.update_error",
                                 model: Organization.model_name.human.downcase,
                                 message: e.message)
    end
  end

  private

  def preimport_checks
    preimport_errors = []
    @tags_lines = {}
    @import.each_line_with_number do |line, number|
      organization_name = line[:name]
      organization_ambiguous = Organization.search(organization_name)
                                           .where("names.value != :name",
                                                   name: organization_name)
      unless organization_ambiguous.empty?
        preimport_errors << {
          line_number: number,
          message: I18n.t("errors.import.ambiguous_name",
                          existing_name: organization_ambiguous.first.name,
                          ambiguous_name: organization_name) }
      end
      tag_names = line[:tag].is_a?(Array) ? line[:tag] : [line[:tag]]
      tag_names.reject!(&:blank?)
      tags = OrganizationTag.where(name: tag_names)
      if tags.count == tag_names.count
        @tags_lines[number] = tags
      else
        missing_tags = tag_names - tags.map(&:name)
        preimport_errors << { line_number: number, message: I18n.t("errors.import.unknown_tags", names: missing_tags.join(", ")) }
      end
    end
    preimport_errors
  end

  def perform_import
    import_errors = []
    @import.each_line_with_number do |line, number|
      begin
        create_or_update_attrs = {}
        if line[:date_of_foundation].present?
          create_or_update_attrs[:date_of_foundation] = line[:date_of_foundation]
          create_or_update_attrs[:approximate_dates] = line[:approximate_dates].present?
        end
        if line[:date_of_extinction].present?
          create_or_update_attrs[:date_of_extinction] = line[:date_of_extinction]
          create_or_update_attrs[:approximate_dates] = line[:approximate_dates].present?
        end
        create_or_update_attrs[:school] = line[:school].present?
        create_or_update_attrs[:tags] = @tags_lines[number]
        create_or_update_organization = CreateOrUpdateOrganization.new(
          line[:name], create_or_update_attrs
        )
        @import.log_info(create_or_update_organization.run, number)
      rescue ImportError => e
        import_errors << { line_number: number, message: e.message }
      rescue => e
        Rails.logger.error("#{e.class}: #{e.message}")
        Rails.logger.debug(e.backtrace.join("\n"))
        raise ProcessLineError.new(number, e)
      end
    end
    import_errors
  end
end
