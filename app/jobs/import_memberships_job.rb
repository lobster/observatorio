class ImportMembershipsJob < ImportJob
  class CreateOrUpdatePerson
    def initialize(name, attributes)
      @name = name
      @attributes = attributes
    end

    def run
      person = Person.find_by_main_name(@name)
      person.present? ? update(person) : create
    end

    private

    def create
      person_ambiguous = Person.search(@name)
                               .where("names.value != :name",
                                      name: @name)
      unless person_ambiguous.empty?
        raise ImportError, I18n.t("errors.import.ambiguous_name",
                                  existing_name: person_ambiguous.first.name,
                                  ambiguous_name: @name)
      end
      person = Person.create!(@attributes.merge(name: @name))
      I18n.t("observatorio.import.logs.person_created",
             person: "#{person.name} (#{person.id})")
    rescue ActiveRecord::RecordInvalid => e
      raise ImportError, I18n.t("errors.import.create_error",
                                 model: Person.model_name.human.downcase,
                                 message: e.message)
    end

    def update(person)
      if @attributes.key?(:tags)
        @attributes[:tags] += person.tags
      end
      person.assign_attributes(@attributes)
      return nil unless person.changed?
      changes = person.changes
                      .keys
                      .map(&:to_sym)
                      .map { |field| Person.human_attribute_name(field) }
      person.save!
      I18n.t("observatorio.import.logs.person_updated",
             person: "#{person.name} (#{person.id})",
             changes: changes.join(", "))
    rescue ActiveRecord::RecordInvalid => e
      raise ImportError, I18n.t("errors.import.update_error",
                                 model: Person.model_name.human.downcase,
                                 message: e.message)
    end
  end

  class CreateMembership
    def initialize(member_name, attributes, links_attributes)
      @member_name = member_name
      @attributes = attributes
      @links_attributes = links_attributes
    end

    def run
      member = Person.find_by_main_name(@member_name)
      membership = OrganizationMember.create!(@attributes.merge(member: member))
      if @links_attributes[:label] || @links_attributes[:url]
        link = Link.new(@links_attributes.merge(linkable: membership))
        link.validate
        link.signature = link.generate_signature
        link.save!
      end
      I18n.t("observatorio.import.logs.membership_created",
             membership: "#{membership.member.name} | #{membership.organization.name} (#{membership.id})")
    rescue ActiveRecord::RecordInvalid => e
      raise ImportError, I18n.t("errors.import.create_error",
                                 model: OrganizationMember.model_name.human.downcase,
                                 message: e.message)
    end
  end

  private

  def preimport_checks
    preimport_errors = []
    @organizations = {}
    @positions = {}
    @tags_lines = {}
    @import.each_line_with_number do |line, number|
      person_first_name = line[:person_first_name]
      person_last_name = line[:person_last_name]
      person_name = "#{person_first_name} #{person_last_name}"
      person_ambiguous = Person.search(person_name)
                               .where("names.value != :name", name: person_name)
      unless person_ambiguous.empty?
        preimport_errors << {
          line_number: number,
          message: I18n.t("errors.import.ambiguous_name",
                          existing_name: person_ambiguous.first.name,
                          ambiguous_name: person_name) }
      end
      organization_name = line[:organization_name]
      unless @organizations[organization_name].present?
        organization = Organization.find_by_main_name(organization_name)
        if organization.present?
          @organizations[organization_name] = organization
        else
          preimport_errors << { line_number: number, message: I18n.t("errors.import.unknown_organization", name: organization_name) }
        end
      end
      position_name = line[:membership_position]
      unless @positions[position_name].present?
        position = OrganizationPosition.find_by_name(position_name)
        if position.present?
          @positions[position_name] = position
        else
          preimport_errors << { line_number: number, message: I18n.t("errors.import.unknown_position", name: position_name) }
        end
      end
      tag_names = line[:person_tag].is_a?(Array) ? line[:person_tag] : [line[:person_tag]]
      tag_names.reject!(&:blank?)
      tags = PersonTag.where(name: tag_names)
      if tags.count == tag_names.count
        @tags_lines[number] = tags
      else
        missing_tags = tag_names - tags.map(&:name)
        preimport_errors << { line_number: number, message: I18n.t("errors.import.unknown_tags", names: missing_tags.join(", ")) }
      end
    end
    preimport_errors
  end

  def perform_import
    import_errors = []
    @import.each_line_with_number do |line, number|
      begin
        # Create or update
        create_or_update_attrs = {}
        if line[:person_date_of_birth].present?
          create_or_update_attrs[:date_of_birth] = line[:person_date_of_birth]
          create_or_update_attrs[:approximate_dates] = line[:person_approximate_dates].present?
        end
        if line[:person_date_of_death].present?
          create_or_update_attrs[:date_of_death] = line[:person_date_of_death]
          create_or_update_attrs[:approximate_dates] = line[:person_approximate_dates].present?
        end
        create_or_update_attrs[:tags] = @tags_lines[number]
        person_name = "#{line[:person_first_name]} #{line[:person_last_name]}"
        create_or_update_person = CreateOrUpdatePerson.new(
          person_name, create_or_update_attrs
        )
        @import.log_info(create_or_update_person.run, number)
        # Create membership
        organization = @organizations[line[:organization_name]]
        create_membership = CreateMembership.new(
          person_name,
          { organization: organization,
            position: @positions[line[:membership_position]],
            location: (organization.headquarters if line[:membership_location_headquarters].present?),
            from_date: line[:membership_from_date],
            to_date: line[:membership_to_date],
            approximate_dates: line[:membership_approximate_dates].present? },
          { label: line[:membership_source_label],
            url: line[:membership_source_url] }
        )
        @import.log_info(create_membership.run, number)
      rescue ImportError => e
        import_errors << { line_number: number, message: e.message }
      rescue => e
        Rails.logger.error("#{e.class}: #{e.message}")
        Rails.logger.debug(e.backtrace.join("\n"))
        raise ProcessLineError.new(number, e)
      end
    end
    import_errors
  end
end
