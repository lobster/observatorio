describe ImportMembershipsJob do
  before do
    content = double(
      file: File.new(File.join(Rails.root, "spec", "files", "beethoven.pdf")),
      visited_at: DateTime.now,
      extension: ".pdf",
      success?: true
    )
    allow(DOWNLOADER).to receive(:head).and_return(content)
    allow(DOWNLOADER).to receive(:get).and_return(content)
  end
  describe "#perform" do
    let(:import) { FactoryBot.create(:memberships_import).reload }
    let!(:organization) { FactoryBot.create(:organization, name: "Silveira Associados") }
    let!(:tag1) { FactoryBot.create(:tag, :person, name: "pertinente") }
    let!(:tag2) { FactoryBot.create(:tag, :person, name: "estimulado") }
    let!(:position1) { FactoryBot.create(:organization_position, name: "Presidente") }
    let!(:position2) { FactoryBot.create(:organization_position, name: "Diretor") }

    subject { lambda { described_class.perform_now(import.id) } }

    it { should change { import.reload.status }.to("success") }
    it { should change(Person, :count).to(1) }
    it { should change(OrganizationMember, :count).to(2) }
    it { should_not change(Organization, :count) }
    it { should_not change(OrganizationPosition, :count) }
    it { should_not change(PersonTag, :count) }
    context "when person exists" do
      let!(:person) { FactoryBot.create(:person, name: "Alberto Silveira") }
      it { should change { import.reload.status }.to("success") }
      it { should_not change(Person, :count) }
      it { should change(OrganizationMember, :count).to(2) }
      it { should change{ person.reload.tags.size }.from(0).to(2) }
      it { should change{ person.reload.date_of_birth } }
      context "and update is invalid" do # This test gonna fail on the year 4955
        let(:import) do
          FactoryBot.create(:memberships_import,
            file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_update_person_invalid.ods"))
          )
        end
        it { should change { import.reload.status }.to("error") }
        it { should_not change { person.reload.tags.size } }
        it { should_not change { person.reload.date_of_birth } }
      end
    end
    context "when person is invalid" do
      let(:import) do
        FactoryBot.create(
          :memberships_import,
          file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_create_person_invalid.ods"))
        )
      end
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when name is ambiguous" do
      before { FactoryBot.create(:person, name: "Alberto Silveiras") }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when name is ambiguous in file" do
      let(:import) do
        FactoryBot.create(
          :memberships_import,
          file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_ambiguous_name.ods"))
        )
      end
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when source is invalid" do
      let(:import) do
        FactoryBot.create(
          :memberships_import,
          file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_invalid_source.ods"))
        )
      end
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when organization does not exist" do
      let!(:organization) { FactoryBot.create(:organization) }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when position does not exist" do
      let!(:position1) { FactoryBot.create(:organization_position) }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
    context "when tag does not exit" do
      let!(:tag1) { FactoryBot.create(:tag, :person) }
      let!(:tag2) { FactoryBot.create(:tag, :person) }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Person, :count) }
      it { should_not change(OrganizationMember, :count) }
    end
  end

  describe "#track_status" do
    let(:import) { FactoryBot.create(:memberships_import) }
    let(:job) { described_class.new(import.id) }
    subject { lambda { job.send(:track_status) { import } } }
    it { should change { import.reload.status }.to("success") }
    context "when processing fails" do
      subject { lambda { job.send(:track_status) { raise "process error" } } }
      it { should change { import.reload.status }.to("error") }
    end
  end
end
