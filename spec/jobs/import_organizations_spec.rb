describe ImportOrganizationsJob do
  describe "#perform" do
    let(:import) { FactoryBot.create(:organizations_import).reload }
    let!(:tag1) { FactoryBot.create(:tag, :organization, name: "superlativa") }
    let!(:tag2) { FactoryBot.create(:tag, :organization, name: "social") }

    subject { lambda { described_class.perform_now(import.id) } }

    it { should change { import.reload.status }.to("success") }
    it { should change(Organization, :count).to(1) }
    it { should_not change(OrganizationTag, :count) }
    context "when organization exists" do
      let!(:organization) { FactoryBot.create(:organization, name: "Awesome Organization") }
      it { should change { import.reload.status }.to("success") }
      it { should_not change(Organization, :count) }
      it { should change{ organization.reload.tags.size }.from(0).to(2) }
      it { should change{ organization.reload.date_of_foundation } }
      context "and update is invalid" do # This test gonna fail on the year 4955
        let(:import) do
          FactoryBot.create(:organizations_import,
            file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "organizations_import_update_invalid.ods"))
          )
        end
        it { should change { import.reload.status }.to("error") }
        it { should_not change { organization.reload.tags.size } }
        it { should_not change { organization.reload.date_of_foundation } }
      end
    end
    context "when organization is invalid" do
      let(:import) do
        FactoryBot.create(
          :organizations_import,
          file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "organizations_import_invalid.ods"))
        )
      end
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Organization, :count) }
    end
    context "when name is ambiguous" do
      before { FactoryBot.create(:organization, name: "Awesome organizations") }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Organization, :count) }
    end
    context "when name is ambiguous in file" do
      let(:import) do
        FactoryBot.create(
          :organizations_import,
          file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "organizations_import_ambiguous_name.ods"))
        )
      end
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Organization, :count) }
    end
    context "when tag does not exit" do
      let!(:tag1) { FactoryBot.create(:tag, :organization) }
      let!(:tag2) { FactoryBot.create(:tag, :organization) }
      it { should change { import.reload.status }.to("error") }
      it { should_not change(Organization, :count) }
    end
  end

  describe "#track_status" do
    let(:import) { FactoryBot.create(:organizations_import) }
    let(:job) { described_class.new(import.id) }
    subject { lambda { job.send(:track_status) { import } } }
    it { should change { import.reload.status }.to("success") }
    context "when processing fails" do
      subject { lambda { job.send(:track_status) { raise "process error" } } }
      it { should change { import.reload.status }.to("error") }
    end
  end
end
