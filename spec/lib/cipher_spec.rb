describe Cipher do
  let(:secret_key) { Base64.decode64("TDExCRRJC2JFwMowOKRamm3f3HYtQ1yjNcYK0rMM/54=") }
  let(:encryption_iv) { Base64.decode64("APSVOGznteUFBnrZoRWbTw==") }
  let(:cipher) { described_class.new(secret_key) }

  describe "#encrypt" do
    subject { cipher.encrypt("clear text", encryption_iv) }
    it { should eq(Base64.decode64("6MGnZui3rsCTwsnBkIrIcQ==")) }
  end

  describe "#decrypt" do
    subject { cipher.decrypt(Base64.decode64("6MGnZui3rsCTwsnBkIrIcQ=="), encryption_iv) }
    it { should eq("clear text") }
  end
end
