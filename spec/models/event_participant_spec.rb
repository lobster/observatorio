describe EventParticipant do
  subject { FactoryBot.build(:event_participant) }

  it { should validate_presence_of(:event) }
  it { should validate_presence_of(:participant) }

  it "should validate duplicate" do
    existing = FactoryBot.create(:event_participant,
                                 from_datetime: Date.new(1990, 7, 3),
                                 to_datetime: Date.new(1997, 9, 1))
    subject = FactoryBot.build(:event_participant,
                               event: existing.event,
                               from_datetime: existing.from_datetime,
                               to_datetime: existing.to_datetime,
                               participant: existing.participant,
                               location: existing.location)
    expect(subject).not_to be_valid
    subject.location = FactoryBot.create(:location)
    expect(subject).to be_valid
    subject.location = existing.location
    subject.from_datetime = Date.new(1987, 4, 8)
    expect(subject).not_to be_valid
    subject.to_datetime = Date.new(1989, 10, 4)
    expect(subject).to be_valid
  end

  describe "#from_datetime" do
    let(:event) { FactoryBot.create(:event, started_at: DateTime.new(1990, 7, 3), finished_at: nil) }
    subject { described_class.new(event: event).from_datetime }
    # by default
    it { should eq(event.started_at) }
  end

  describe "#to_datetime" do
    let(:event) { FactoryBot.create(:event, finished_at: DateTime.new(1990, 7, 3), started_at: nil) }
    subject { described_class.new(event: event).to_datetime }
    # by default
    it { should eq(event.finished_at) }
  end

  describe "#approximate_dates" do
    let(:event) { FactoryBot.create(:event, approximate_dates: random_bool) }
    subject { described_class.new(event: event).approximate_dates }
    # by default
    it { should eq(event.approximate_dates) }
  end

  describe "#time_zone" do
    let(:event) { FactoryBot.create(:event, time_zone: random_tz) }
    subject { described_class.new(event: event).time_zone }
    # by default
    it { should eq(event.time_zone) }
  end

  describe "#from_datetime_in_time_zone" do
    let(:event_participant) { FactoryBot.build(:event_participant, from_datetime: Time.now - 10.days, time_zone: random_tz) }
    subject { event_participant.from_datetime_in_time_zone }
    it { should eq(event_participant.from_datetime.in_time_zone(event_participant.time_zone)) }
  end

  describe "#to_datetime_in_time_zone" do
    let(:event_participant) { FactoryBot.build(:event_participant, to_datetime: Time.now - 10.days, time_zone: random_tz) }
    subject { event_participant.to_datetime_in_time_zone }
    it { should eq(event_participant.to_datetime.in_time_zone(event_participant.time_zone)) }
  end
end
