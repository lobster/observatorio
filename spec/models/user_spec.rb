describe User do
  subject { FactoryBot.build(:user) }

  it { should have_many(:notes)
                .inverse_of(:author)
                .with_foreign_key("author_id")
                .dependent(:destroy) }

  let(:user) { described_class.new }

  describe "#display_name" do
    let(:user) { FactoryBot.build(:user) }
    subject { user.display_name }
    it { should eq(user.email) }
  end
end
