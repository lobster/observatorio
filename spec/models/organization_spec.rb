describe Organization do
  subject { FactoryBot.build(:organization) }

  it { should have_many(:members)
                .class_name(OrganizationMember)
                .inverse_of(:organization)
                .with_foreign_key("organization_id")
                .dependent(:destroy) }

  it { should have_many(:owners)
                .class_name(OrganizationOwner)
                .inverse_of(:organization)
                .with_foreign_key("organization_id")
                .dependent(:destroy) }

  it { should have_many(:organizations_owner)
                .class_name(OrganizationOwner)
                .inverse_of(:owner)
                .with_foreign_key("owner_id")
                .dependent(:destroy) }

  it { should have_many(:departments)
                .class_name(OrganizationDepartment)
                .inverse_of(:organization)
                .with_foreign_key("organization_id")
                .dependent(:destroy) }

  it { should have_many(:students)
                .class_name(Education)
                .inverse_of(:school)
                .with_foreign_key("school_id")
                .dependent(:destroy) }

  it { should have_many(:events_participant)
                .class_name(EventParticipant)
                .inverse_of(:participant)
                .with_foreign_key("participant_id")
                .dependent(:destroy) }
  it { should have_many(:tagged_organizations)
                .class_name(TaggedOrganization)
                .inverse_of(:organization)
                .with_foreign_key("organization_id") }
  it { should have_many(:tags)
                .class_name(OrganizationTag)
                .through(:tagged_organizations)
                .dependent(:destroy) }

  it { should have_many(:notes) }
  it { should have_many(:attachments) }
  it { should have_many(:links) }

  it { should validate_presence_of(:main_name) }

  it { should allow_value("58697297000176").for(:cnpj) }
  it { should allow_value(nil).for(:cnpj) }
  it { should_not allow_value("58697247000176").for(:cnpj) }

  it { expect(described_class.new.school).to be(false) }

  it "should validate only schools can have students" do
    subject.save!
    subject.school = false
    subject.students << FactoryBot.build(:education, school: subject)
    expect(subject).not_to be_valid
    subject.school = true
    expect(subject).to be_valid
  end

  it "should validate uniqueness of CNPJ" do
    existing = FactoryBot.create(:organization, cnpj: "58697297000176")
    organization = FactoryBot.build(:organization, cnpj: existing.cnpj)
    expect(organization).not_to be_valid
    organization.cnpj = "50965031000155"
    expect(organization).to be_valid
  end

  it "should normalize CNPJ" do
    organization = FactoryBot.build(:organization, cnpj: "58.697.297/0001-76")
    expect { organization.save! }.to change(organization, :cnpj).to("58697297000176")
  end

  describe "#formatted_cnpj" do
    let(:organization) { FactoryBot.create(:organization, cnpj: "58697297000176") }
    subject { organization.formatted_cnpj }
    it { should eq("58.697.297/0001-76") }
    context "when CNPJ is nil" do
      let(:organization) { FactoryBot.create(:organization, cnpj: nil) }
      it { should be(nil) }
    end
  end

  describe "#valid" do
    let(:organization) { FactoryBot.create(:organization) }
    let(:organization_owner1) do
      FactoryBot.build(:organization_owner,
                        organization: organization,
                        portion: rand(0.1..0.4))
    end
    let(:organization_owner2) do
      FactoryBot.build(:organization_owner,
                        organization: organization,
                        portion: rand(0.1..0.6))
    end
    subject { organization.valid? }
    before do
      organization.owners << organization_owner1
      organization.owners << organization_owner2
    end
    it { should be true }
    describe "when cumulated portion is higher than 1.0" do
      let(:organization_owner1) do
        FactoryBot.build(:organization_owner,
                          organization: organization,
                          portion: rand(0.5..1.0))
      end
      let(:organization_owner2) do
        FactoryBot.build(:organization_owner,
                          organization: organization,
                          portion: rand(0.6..1.0))
      end
      it { should be false }
    end
  end

  describe ".school" do
    let!(:organization_school) { FactoryBot.create(:organization, school: true) }
    let!(:organization_not_school) { FactoryBot.create(:organization, school: false) }
    subject { described_class.school }
    it { should contain_exactly(organization_school) }
  end

  describe ".order_by_main_name" do
    let!(:organization1) { FactoryBot.create(:organization, name: "AACD") }
    let!(:organization2) { FactoryBot.create(:organization, name: "Saúde Criança") }
    let!(:organization3) { FactoryBot.create(:organization, name: "Centro de Inclusão Digital") }

    before do
      FactoryBot.create(:name, value: "AAA", nameable: organization2)
    end

    subject { described_class.order_by_main_name }

    it { should eq([organization1, organization3, organization2]) }
  end

  describe ".search.order_by_main_name" do
    let!(:organization1) { FactoryBot.create(:organization, name: "AACD") }
    let!(:organization2) { FactoryBot.create(:organization, name: "Saúde Criança") }
    let!(:organization3) { FactoryBot.create(:organization, name: "Centro de Inclusão Digital") }

    before do
      FactoryBot.create(:name, value: "Centro Digital", nameable: organization2)
    end

    subject { described_class.search("Centro").order_by_main_name }

    it { should eq([organization3, organization2]) }
  end
end
