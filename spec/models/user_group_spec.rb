describe UserGroup do
  subject { FactoryBot.build(:user_group) }

  it { should validate_uniqueness_of(:name) }
end
