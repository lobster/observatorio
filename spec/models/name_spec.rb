describe Name do
  subject { FactoryBot.build(:name) }

  it { should validate_presence_of(:value) }
  it { should validate_presence_of(:nameable) }

  context "main" do
    subject { FactoryBot.build(:name, main: true) }
    it { should validate_uniqueness_of(:value).scoped_to(:nameable_type).case_insensitive }
    it { should validate_uniqueness_of(:main).scoped_to(:nameable_id, :nameable_type) }
  end

  context "not main" do
    subject { FactoryBot.build(:name, main: false) }
    it { should_not validate_uniqueness_of(:value).scoped_to(:nameable_type) }
  end
end
