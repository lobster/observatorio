describe EducationDegree do
  subject { FactoryBot.build(:education_degree) }

  it { should validate_uniqueness_of(:name).case_insensitive }

  it { should have_many(:educations)
                .class_name(Education)
                .inverse_of(:degree)
                .with_foreign_key("degree_id")
                .dependent(:restrict_with_error) }
end
