describe Import do
  subject { FactoryBot.build(:memberships_import) }

  it { should validate_presence_of :user }
  it { should validate_inclusion_of(:status).in_array(%w(pending processing success error)) }

  it "should validate file" do
    # Invalid format
    subject = FactoryBot.build(
      :memberships_import,
      file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg"))
    )
    expect(subject).not_to be_valid
    # Missing headers
    subject = FactoryBot.build(
      :memberships_import,
      file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_missing_headers.ods"))
    )
    expect(subject).not_to be_valid
    # Ok
    subject = FactoryBot.build(
      :memberships_import,
      file_unencrypted: File.new(File.join(Rails.root, "spec", "files", "memberships_import_ok.ods"))
    )
    expect(subject).to be_valid
  end

  describe "#status" do
    subject { described_class.new.status }
    # by default
    it { should eq "pending" }
  end

  describe "#finished?" do
    subject { described_class.new(status: status).finished? }

    %w(success error).each do |finished_status|
      context "when status is #{finished_status}" do
        let(:status) { finished_status }
        it { should be true }
      end
    end

    context "otherwise" do
      let(:status) { random_string }
      it { should be false }
    end
  end
end
