describe Person do
  subject { FactoryBot.build(:person) }

  it { should have_many(:organizations_member)
                .class_name(OrganizationMember)
                .inverse_of(:member)
                .with_foreign_key("member_id")
                .dependent(:destroy) }

  it { should have_many(:organizations_owner)
                .class_name(OrganizationOwner)
                .inverse_of(:owner)
                .with_foreign_key("owner_id")
                .dependent(:destroy) }

  it { should have_many(:relationships)
                .class_name(Relationship)
                .inverse_of(:owner)
                .with_foreign_key("owner_id")
                .dependent(:destroy) }

  it { should have_many(:inverse_relationships)
                .class_name(Relationship)
                .inverse_of(:with)
                .with_foreign_key("with_id")
                .dependent(:destroy) }

  it { should have_many(:educations)
                .class_name(Education)
                .inverse_of(:student)
                .with_foreign_key("student_id")
                .dependent(:destroy) }

  it { should have_many(:events_participant)
                .class_name(EventParticipant)
                .inverse_of(:participant)
                .with_foreign_key("participant_id")
                .dependent(:destroy) }
  it { should have_many(:tagged_people)
                .class_name(TaggedPerson)
                .inverse_of(:person)
                .with_foreign_key("person_id") }
  it { should have_many(:tags)
                .class_name(PersonTag)
                .through(:tagged_people)
                .dependent(:destroy) }

  it { should have_many(:notes) }
  it { should have_many(:attachments) }
  it { should have_many(:links) }

  it "should validates nationalities are ISO3166 alpha2 country codes" do
    subject.nationalities = ["BR", "FR"]
    expect(subject).to be_valid
    subject.nationalities = ["BR", "INVALID"]
    expect(subject).not_to be_valid
  end

  it "should remove duplicate nationalities" do
    subject.nationalities = ["BR", "BR"]
    subject.save!
    expect(subject.nationalities).to contain_exactly("BR")
  end

  it "should sort nationalities" do
    subject.nationalities = ["FR", "BR"]
    subject.save!
    expect(subject.nationalities).to eq(["BR", "FR"])
  end

  describe ".order_by_main_name" do
    let!(:person1) { FactoryBot.create(:person, name: "Alessandra Bentes") }
    let!(:person2) { FactoryBot.create(:person, name: "Vitor Oliveira") }
    let!(:person3) { FactoryBot.create(:person, name: "Bruno Moreira") }

    before do
      FactoryBot.create(:name, value: "Abrãao Oliveira", nameable: person2)
    end

    subject { described_class.order_by_main_name }

    it { should eq([person1, person3, person2]) }
  end

  describe ".search.order_by_main_name" do
    let!(:person1) { FactoryBot.create(:person, name: "Alessandra Bentes") }
    let!(:person2) { FactoryBot.create(:person, name: "Vitor Oliveira") }
    let!(:person3) { FactoryBot.create(:person, name: "Bruno Moreira") }

    before do
      FactoryBot.create(:name, value: "Abrãao Moreira", nameable: person2)
    end

    subject { described_class.search("Moreira").order_by_main_name }

    it { should eq([person3, person2]) }
  end
end
