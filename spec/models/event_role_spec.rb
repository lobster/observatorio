describe EventRole do
  subject { FactoryBot.build(:event_role) }

  it { should validate_uniqueness_of(:name).case_insensitive }

  it { should have_many(:event_participants)
                .class_name(EventParticipant)
                .inverse_of(:role)
                .with_foreign_key("role_id")
                .dependent(:restrict_with_error) }
end
