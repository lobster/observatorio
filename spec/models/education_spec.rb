describe Education do
  subject { FactoryBot.build(:education) }

  it { should validate_presence_of(:student) }
  it { should validate_presence_of(:school) }

  it "should validate school is really a school" do
    allow(subject.school).to receive(:school?).and_return(false)
    expect(subject).not_to be_valid
    allow(subject.school).to receive(:school?).and_return(true)
    expect(subject).to be_valid
  end
end
