describe HasImmutableAttributes do
  class described_class::TestModel < ApplicationRecord
    has_immutable_attributes :immutable
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.text    :immutable, null: false
  end

  it "should validate attribute is immutable" do
    subject = described_class::TestModel.create!(immutable: SecureRandom.hex)
    subject.immutable = "changed"
    expect(subject).not_to be_valid
  end
end
