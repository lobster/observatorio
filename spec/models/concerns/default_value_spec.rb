describe DefaultValue do
  class described_class::TestModel < ApplicationRecord
    default_value :string_attribute, "default_name"
    default_value :integer_attribute, 3
    default_value :boolean_attribute, false
    default_value :dynamic_attribute, lambda { |o| "lambda value" }
  end
  create_test_model_table described_class::TestModel, id: :uuid do |t|
    t.text    :string_attribute
    t.integer :integer_attribute
    t.boolean :boolean_attribute
    t.text    :dynamic_attribute
  end
  subject { described_class::TestModel.new }

  it { expect(subject.string_attribute).to eq("default_name") }
  it { expect(subject.integer_attribute).to eq(3) }
  it { expect(subject.boolean_attribute).to be(false) }
  it { expect(subject.dynamic_attribute).to eq("lambda value") }

  describe "when given values" do
    let(:string_value) { random_string }
    let(:integer_value) { random_int }
    let(:boolean_value) { random_bool }
    subject { described_class::TestModel.new(string_attribute: string_value,
                                             integer_attribute: integer_value,
                                             boolean_attribute: boolean_value) }

    it { expect(subject.string_attribute).to eq(string_value) }
    it { expect(subject.integer_attribute).to eq(integer_value) }
    it { expect(subject.boolean_attribute).to be(boolean_value) }
  end
end
