describe Strip do
  class described_class::TestModelStripped < ApplicationRecord
    strip :name
  end
  create_test_model_table described_class::TestModelStripped, id: :uuid do |t|
    t.text :name
  end
  
  it "should strip name" do
    subject = described_class::TestModelStripped.new(name: " maria ")
    expect { subject.validate }.to change(subject, :name).to("maria")
  end
end
