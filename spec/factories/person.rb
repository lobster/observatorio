FactoryBot.define do
  factory :person do
    trait :with_picture do
      after(:create) do |person|
        FactoryBot.create(:picture, picturable: person)
      end
    end
    nationalities { Country.all.sample(rand(1..3)).map(&:alpha2) }
    after(:build) do |person|
      person.main_name ||= FactoryBot.build(:name, nameable: person, main: true)
    end
  end
end
