FactoryBot.define do
  factory :event_role do
    name { "Name-#{SecureRandom.hex}" }
  end
end
