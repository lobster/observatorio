FactoryBot.define do
  factory :organization_member do
    organization { |o| o.association(:organization) }
    member { |o| o.association(:person) }
    position { |o| o.association(:organization_position) }
    from_date { Date.today - rand(20..40).days }
    to_date { Date.today - rand(1..15).days }
    approximate_dates { [true, false].sample }
  end
end
