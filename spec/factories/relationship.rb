FactoryBot.define do
  factory :relationship do
    owner { |o| o.association(:person) }
    with { |o| o.association(:person) }
    degree { Relationship::DEGREES.sample }
  end

  factory :relationship_date_boundaried, parent: :relationship do
    degree { (Relationship::DEGREES - Relationship::DEGREES_ETERNAL).sample }
    from_date { Date.today - rand(50..300).days }
    to_date { Date.today - rand(10..30).days }
    approximate_dates { [true, false].sample }
  end
end
