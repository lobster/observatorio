FactoryBot.define do
  factory :education do
    student { |o| o.association(:person) }
    school { |o| o.association(:organization, :school) }
  end
end
