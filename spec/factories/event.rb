FactoryBot.define do
  factory :event do
    name { "Name-#{SecureRandom.hex}" }
    time_zone { ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name).sample }
    started_at { DateTime.now - rand(50..100).days }
    finished_at { DateTime.now - rand(10..40).days }
    approximate_dates { [true, false].sample }
  end
end
