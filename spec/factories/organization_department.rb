FactoryBot.define do
  factory :organization_department do
    name { "Name-#{SecureRandom.hex}" }
    organization { |o| o.association(:organization) }
  end
end
