FactoryBot.define do
  factory :picture do
    file { File.new(File.join(Rails.root, "spec", "files", "crucified_barbara.jpg")) }
    picturable { |o| o.association(:person) }
  end
end
