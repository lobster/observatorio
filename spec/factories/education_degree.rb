FactoryBot.define do
  factory :education_degree do
    name { "Degree-#{SecureRandom.hex}" }
  end
end
