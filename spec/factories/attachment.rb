FactoryBot.define do
  factory :attachment do
    trait :pdf do
      file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "beethoven.pdf")) }
      file_extension { ".pdf" }
    end
    trait :image do
      file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "crucified_barbara.jpg")) }
      file_extension { ".jpg" }
    end
    trait :video do
      file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "pexels_com_seashore-1722595.mp4")) }
      file_extension { ".mp4" }
    end
    trait :audio do
      file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "example.mp3")) }
      file_extension { ".mp3" }
    end
    label { "RandomString-#{SecureRandom.hex}" }
    attachable { |o| o.association(:person) }
    file_unencrypted { File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg")) }
    file_extension { ".jpg" }
  end
end
