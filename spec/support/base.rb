require 'active_support/concern'

module Support
  module Base
    extend ActiveSupport::Concern

    class_methods do
      def create_test_model_table(test_model_class, options = {}, &block)
        before :all do
          m = ActiveRecord::Migration.new
          m.verbose = false
          m.create_table(test_model_class.table_name.to_sym, options, &block)
          test_model_class.reset_column_information
        end
        after :all do
          m = ActiveRecord::Migration.new
          m.verbose = false
          m.drop_table(test_model_class.table_name.to_sym)
        end
      end
    end

    def random_date
        Date.today - rand(10..3_650).days
    end

    def random_datetime
      random_date.to_datetime + rand(0..1.day.seconds).seconds
    end

    def random_lat
      rand(-50.0..-10.0).truncate(8)
    end

    def random_lng
      rand(50.0..90.0).truncate(8)
    end

    def random_locale
      I18n.available_locales.sample
    end

    def random_string
      SecureRandom.hex
    end

    def random_int
      rand(-500..500)
    end

    def random_positive_int
      rand(100..5000)
    end

    def random_bool
      [true, false].sample
    end

    def random_url
      "https://#{SecureRandom.hex}.net"
    end

    def random_tz
      ActiveSupport::TimeZone.all.map(&:tzinfo).map(&:name).sample
    end

    def random_extension
      Rack::Mime::MIME_TYPES.keys.sample
    end
  end
end

RSpec.configure do |config|
  config.include Support::Base
end
