module Support
  module Nominatim
    def reverse_ok
      Oj.load(File.read(File.expand_path("spec/support/fixtures/nominatim/reverse.ok.json")))
    end

    def reverse_ok_pedestrian
      Oj.load(File.read(File.expand_path("spec/support/fixtures/nominatim/reverse.ok.pedestrian.json")))
    end

    def reverse_ok_construction
      Oj.load(File.read(File.expand_path("spec/support/fixtures/nominatim/reverse.ok.construction.json")))
    end

    def reverse_ok_address26
      Oj.load(File.read(File.expand_path("spec/support/fixtures/nominatim/reverse.ok.address26.json")))
    end

    def reverse_not_found
      Oj.load(File.read(File.expand_path("spec/support/fixtures/nominatim/reverse.not_found.json")))
    end
  end
end

RSpec.configure do |config|
  # Include helper in specs with ":google" group
  config.include Support::Nominatim, :nominatim
end
