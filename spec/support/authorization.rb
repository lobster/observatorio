module Support
  module Authorization
    def init_authorization
      allow_any_instance_of(Ability).to receive(:can?).and_call_original
    end

    def authorize_all
      allow_any_instance_of(Ability).to receive(:can?).and_return(true)
    end

    def authorize(action, subject)
      allow_any_instance_of(Ability).to receive(:can?)
                                          .with(action, subject)
                                          .and_return(true)
    end

    def unauthorize(action, subject)
      allow_any_instance_of(Ability).to receive(:can?)
                                          .with(action, subject)
                                          .and_return(false)
    end
  end
end

RSpec.configure do |config|
  config.include Support::Authorization, type: :request
end
