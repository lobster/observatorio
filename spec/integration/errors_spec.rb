describe "errors" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
  end

  it "should respond not found" do
    get "/404"
    expect(response).to have_http_status(:not_found)
    expect(response).to render_template(:not_found)
  end

  it "should respond internal server error" do
    get "/500"
    expect(response).to have_http_status(:internal_server_error)
    expect(response).to render_template(:internal_server_error)
  end
end
