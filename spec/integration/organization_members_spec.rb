describe "organizations members" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # ORGANIZATION MEMBERS INDEX
  describe "GET /organization_members" do
    let!(:member) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    let!(:organization_member1) do
      FactoryBot.create(:organization_member,
        member: member,
        organization: organization,
        department: department
      )
    end
    let!(:organization_member2) do
      FactoryBot.create(:organization_member,
        member: member,
        organization: organization
      )
    end
    let!(:organization_member3) do
      FactoryBot.create(:organization_member, member: member)
    end
    let!(:organization_member4) do
      FactoryBot.create(:organization_member, organization: organization)
    end
    before do
      authorize :read, member
      authorize :read, organization
    end
    it "should list organizations of a member" do
      get "/organization_members", params: {
        organization_member: { member_id: member.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_member1.id)
      expect(response.body).to include(organization_member2.id)
      expect(response.body).to include(organization_member3.id)
      expect(response.body).not_to include(organization_member4.id)
    end
    it "should list members of an organization" do
      get "/organization_members", params: {
        organization_member: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_member1.id)
      expect(response.body).to include(organization_member2.id)
      expect(response.body).not_to include(organization_member3.id)
      expect(response.body).to include(organization_member4.id)
    end
    it "should list members of a department" do
      get "/organization_members", params: {
        organization_member: { department_id: department.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(organization_member1.id)
      expect(response.body).not_to include(organization_member2.id)
      expect(response.body).not_to include(organization_member3.id)
      expect(response.body).not_to include(organization_member4.id)
    end
    it "should list members of an organization with no department" do
      get "/organization_members", params: {
        organization_member: {
          organization_id: organization.id,
          department_id: ""
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).not_to include(organization_member1.id)
      expect(response.body).to include(organization_member2.id)
      expect(response.body).not_to include(organization_member3.id)
      expect(response.body).to include(organization_member4.id)
    end
    context "when neither member_id nor organization_id given" do
      it "should respond bad request" do
        get "/organization_members"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read member" do
      before { unauthorize :read, member }
      it "should respond forbidden" do
        get "/organization_members", params: {
          organization_member: { member_id: member.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read organization" do
      before { unauthorize :read, organization }
      it "should respond forbidden" do
        get "/organization_members", params: {
          organization_member: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW ORGANIZATION MEMBER FORM
  describe "GET /organization_members/new" do
    let!(:member) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    before do
      authorize :update, member
      authorize :update, organization
    end
    it "should render new organization_member form for member" do
      get "/organization_members/new", params: {
        organization_member: { member_id: member.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    it "should render new organization_member form for organization" do
      get "/organization_members/new", params: {
        organization_member: { organization_id: organization.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when no member nor organization is given" do
      it "should respond bad request" do
        get "/organization_members/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to update member" do
      before { unauthorize :update, member }
      it "should respond forbidden" do
        get "/organization_members/new", params: {
          organization_member: { member_id: member.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should respond forbidden" do
        get "/organization_members/new", params: {
          organization_member: { organization_id: organization.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE ORGANIZATION MEMBER
  describe "POST /organization_members" do
    let(:member) { FactoryBot.create(:person) }
    let(:organization) { FactoryBot.create(:organization) }
    let(:position) { FactoryBot.create(:organization_position) }
    let(:department) { FactoryBot.create(:organization_department, organization: organization) }
    let(:from_date) { random_date }
    let(:to_date) { from_date + 1.day }
    let(:approximate_dates) { [true, false].sample }
    let(:location) { FactoryBot.create(:location) }
    before do
      authorize :update, member
      authorize :update, organization
    end
    it "should create organization member" do
      expect {
        post "/organization_members", params: {
          organization_member: {
            member_id: member.id,
            organization_id: organization.id,
            position_id: position.id,
            department_id: department.id,
            from_date: from_date,
            to_date: to_date,
            approximate_dates: approximate_dates,
            location_id: location.id
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(OrganizationMember, :count).from(0).to(1)
      created = OrganizationMember.first
      expect(created.member).to eq(member)
      expect(created.organization).to eq(organization)
      expect(created.position).to eq(position)
      expect(created.department).to eq(department)
      expect(created.from_date).to eq(from_date)
      expect(created.to_date).to eq(to_date)
      expect(created.approximate_dates).to eq(approximate_dates)
      expect(created.location).to eq(location)
    end
    context "when validation fails" do
      it "should not create organization member" do
        expect {
          post "/organization_members", params: {
            organization_member: {
              member_id: member.id,
              organization_id: organization.id,
              from_date: from_date,
              to_date: to_date,
              approximate_dates: nil
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(OrganizationMember, :count)
      end
    end
    context "when user is not authorized to update member" do
      before { unauthorize :update, member }
      it "should not create organization member" do
        expect {
          post "/organization_members", params: {
            organization_member: {
              member_id: member.id,
              organization_id: organization.id,
              position_id: position.id,
              department_id: department.id,
              from_date: from_date,
              to_date: to_date,
              approximate_dates: approximate_dates,
              location_id: location.id
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationMember, :count)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not create organization member" do
        expect {
          post "/organization_members", params: {
            organization_member: {
              member_id: member.id,
              organization_id: organization.id,
              position_id: position.id,
              department_id: department.id,
              from_date: from_date,
              to_date: to_date,
              approximate_dates: approximate_dates,
              location_id: location.id
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationMember, :count)
      end
    end
  end

  #SHOW ORGANIZATION MEMBER
  describe "GET /organization_members/:id" do
    let(:member) { FactoryBot.create(:person) }
    let(:organization) { FactoryBot.create(:organization) }
    let(:organization_member) do
      FactoryBot.create(:organization_member, member: member, organization: organization)
    end
    before do
      authorize :read, member
      authorize :read, organization
    end
    it "should render organization_member" do
      get "/organization_members/#{organization_member.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
  end

  # EDIT ORGANIZATION MEMBER FORM
  describe "GET /organization_members/:id/edit" do
    let(:member) { FactoryBot.create(:person) }
    let(:organization) { FactoryBot.create(:organization) }
    let!(:department) do
      FactoryBot.create(:organization_department, organization: organization)
    end
    let(:organization_member) do
      FactoryBot.create(:organization_member, member: member, organization: organization)
    end
    before do
      authorize :update, member
      authorize :update, organization
    end
    it "should render edit organization member form" do
      get "/organization_members/#{organization_member.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update member" do
      before { unauthorize :update, member }
      it "should not render edit organization form" do
        get "/organization_members/#{organization_member.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not render edit organization form" do
        get "/organization_members/#{organization_member.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE ORGANIZATION MEMBER
  describe "PUT /organization_members/:id" do
    let!(:member) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_member) do
      FactoryBot.create(:organization_member, member: member, organization: organization)
    end
    let(:new_position) { FactoryBot.create(:organization_position) }
    let(:new_department) { FactoryBot.create(:organization_department, organization: organization) }
    let(:new_location) { FactoryBot.create(:location) }
    let(:new_from_date) { random_date }
    let(:new_to_date) { new_from_date + 1.day }
    let(:new_approximate_dates) { organization_member.approximate_dates.present? ? false : true }
    before do
      authorize :update, member
      authorize :update, organization
    end
    it "should update organization member" do
      expect {
        put "/organization_members/#{organization_member.id}", params: {
          organization_member: {
            position_id: new_position.id,
            department_id: new_department.id,
            from_date: new_from_date,
            to_date: new_to_date,
            approximate_dates: new_approximate_dates,
            location_id: new_location.id
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update)
      }.not_to change(OrganizationMember, :count)
      organization_member.reload
      expect(organization_member.position).to eq(new_position)
      expect(organization_member.department).to eq(new_department)
      expect(organization_member.from_date).to eq(new_from_date)
      expect(organization_member.to_date).to eq(new_to_date)
      expect(organization_member.approximate_dates).to eq(new_approximate_dates)
      expect(organization_member.location).to eq(new_location)
    end
    context "when validation fails" do
      it "should not update organization member" do
        put "/organization_members/#{organization_member.id}", params: {
          organization_member: {
            position_id: new_position.id,
            to_date: new_to_date,
            approximate_dates: nil
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:form)
        organization_member.reload
        expect(organization_member.position).not_to eq(new_position)
      end
    end
    context "when user is not authorized to update member" do
      before { unauthorize :update, member }
      it "should not update organization_member" do
        put "/organization_members/#{organization_member.id}", params: {
          organization_member: {
            position_id: new_position.id,
            department_id: new_department.id,
            from_date: new_from_date,
            to_date: new_to_date,
            approximate_dates: new_approximate_dates,
            location_id: new_location.id
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        organization_member.reload
        expect(organization_member.position).not_to eq(new_position)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not update organization_member" do
        put "/organization_members/#{organization_member.id}", params: {
          organization_member: {
            position_id: new_position.id,
            department_id: new_department.id,
            from_date: new_from_date,
            to_date: new_to_date,
            approximate_dates: new_approximate_dates,
            location_id: new_location.id
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        organization_member.reload
        expect(organization_member.position).not_to eq(new_position)
      end
    end
  end

  # DESTROY ORGANIZATION MEMBER
  describe "DELETE /organization_members/:id" do
    let!(:member) { FactoryBot.create(:person) }
    let!(:organization) { FactoryBot.create(:organization) }
    let!(:organization_member) do
      FactoryBot.create(:organization_member, member: member, organization: organization)
    end
    before do
      authorize :update, member
      authorize :update, organization
    end
    it "should delete organization member" do
      expect {
        delete "/organization_members/#{organization_member.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(OrganizationMember, :count).from(1).to(0)
    end
    context "when user is not authorized to update member" do
      before { unauthorize :update, member }
      it "should not delete organization member" do
        expect {
          delete "/organization_members/#{organization_member.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationMember, :count)
      end
    end
    context "when user is not authorized to update organization" do
      before { unauthorize :update, organization }
      it "should not delete organization member" do
        expect {
          delete "/organization_members/#{organization_member.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(OrganizationMember, :count)
      end
    end
  end
end
