describe "import organizations" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # IMPORTS INDEX
  describe "GET /imports/organizations" do
    let!(:imports) { FactoryBot.create_list(:organizations_import, rand(1..3)) }
    let(:import) { imports.sample }
    before do
      authorize(:list, OrganizationsImport)
      authorize(:read, import)
    end
    it "should list imports" do
      get "/imports/organizations"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(import.display_name)
    end
    context "when user is not authorized to list imports" do
      before { unauthorize(:list, OrganizationsImport) }
      it "should not list imports" do
        get "/imports/organizations"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(import.display_name)
      end
    end
  end

  # SHOW IMPORT
  describe "GET /imports/organizations/:id" do
    let!(:import) { FactoryBot.create(:organizations_import) }
    before { authorize(:read, import) }
    it "should show import" do
      get "/imports/organizations/#{import.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
      expect(response.body).to include(import.display_name)
    end
    context "when user is not authorized to read the import" do
      before { unauthorize(:read, import) }
      it "should redirect to home page" do
        get "/imports/organizations/#{import.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response.body).not_to include(import.display_name)
      end
    end
  end

  # DOWNLOAD IMPORT FILE
  describe "GET /imports/organizations/:id/download" do
    let!(:import) { FactoryBot.create(:organizations_import) }
    before { authorize(:read, import) }
    it "should stream import file" do
      get "/imports/organizations/#{import.id}/download"
      expect(response).to have_http_status(:ok)
    end
    context "when user is not authorized to read the import" do
      before { unauthorize(:read, import) }
      it "should redirect to home page" do
        get "/imports/organizations/#{import.id}/download"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # CREATE IMPORT
  describe "POST /imports/organizations" do
    let(:file) do
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, "spec", "files", "organizations_import_ok.ods"),
        "application/vnd.oasis.opendocument.spreadsheet"
      )
    end
    before { authorize(:create, Import) }
    it "should create import" do
      expect {
        post "/imports/organizations", params: {
          import: { file_unencrypted: file }
        }
      }.to change(Import, :count).from(0).to(1)
      created_import = Import.first
      expect(response).to redirect_to("/imports/organizations/#{created_import.id}")
    end
    context "when validation fails" do
      let(:file) do
        Rack::Test::UploadedFile.new(
          File.join(Rails.root, "spec", "files", "organizations_import_missing_header.ods"),
          "application/vnd.oasis.opendocument.spreadsheet"
        )
      end
      it "should not create import" do
        expect {
          post "/imports/organizations", params: {
            import: { file_unencrypted: file }
          }
          expect(response).to redirect_to("/imports/organizations")
        }.not_to change(Import, :count)
      end
    end
  end
end
