describe "people" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # PEOPLE INDEX
  describe "GET /people" do
    let!(:last_name1) { "de Melo" }
    let!(:last_name2) { "Moreira" }

    let!(:person1) { FactoryBot.create(:person, name: "Alessandra Campos #{last_name1}") }
    let!(:person2) { FactoryBot.create(:person, :with_picture, name: "André James #{last_name1}") }
    let!(:person3) { FactoryBot.create(:person, name: "Bruna #{last_name2}") }
    let!(:person4) { FactoryBot.create(:person, name: "Carlos #{last_name2}") }
    let!(:person5) { FactoryBot.create(:person, name: "Eduardo #{last_name2}") }
    let!(:person6) { FactoryBot.create(:person, name: "Junior #{last_name2}") }
    let!(:person7) { FactoryBot.create(:person, name: "Larissa #{last_name2}") }
    let!(:person8) { FactoryBot.create(:person, name: "Lorena #{last_name2}") }
    let!(:person9) { FactoryBot.create(:person, name: "Marcos #{last_name2}") }
    let!(:person10) { FactoryBot.create(:person, name: "Pedro #{last_name2}") }
    let!(:person11) { FactoryBot.create(:person, name: "Roberta #{last_name2}") }
    let!(:person12) { FactoryBot.create(:person, name: "Vitor #{last_name2}") }

    before { authorize(:list, Person) }
    it "should list only 8 people per page" do
      get "/people"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(person1.name)
      expect(response.body).to include(person2.name)
      expect(response.body).to include(person3.name)
      expect(response.body).to include(person4.name)
      expect(response.body).to include(person5.name)
      expect(response.body).to include(person6.name)
      expect(response.body).to include(person7.name)
      expect(response.body).to include(person8.name)
      expect(response.body).not_to include(person9.name)
      expect(response.body).not_to include(person10.name)
      expect(response.body).not_to include(person11.name)
      expect(response.body).not_to include(person12.name)
    end
    it "should limit search results to 8 people per page" do
      get "/people", params: { search: last_name2 }
      expect(response.body).to include(person3.name)
      expect(response.body).to include(person4.name)
      expect(response.body).to include(person5.name)
      expect(response.body).to include(person6.name)
      expect(response.body).to include(person7.name)
      expect(response.body).to include(person8.name)
      expect(response.body).to include(person9.name)
      expect(response.body).to include(person10.name)
      expect(response.body).not_to include(person11.name)
      expect(response.body).not_to include(person12.name)
    end
    it "should only return 2 people" do
      get "/people", params: { search: last_name1 }
      expect(response.body).to include(person1.name)
      expect(response.body).to include(person2.name)
      expect(response.body).not_to include(person3.name)
      expect(response.body).not_to include(person4.name)
      expect(response.body).not_to include(person5.name)
      expect(response.body).not_to include(person6.name)
      expect(response.body).not_to include(person7.name)
      expect(response.body).not_to include(person8.name)
      expect(response.body).not_to include(person9.name)
      expect(response.body).not_to include(person10.name)
      expect(response.body).not_to include(person11.name)
      expect(response.body).not_to include(person12.name)
    end
    context "when user is not authorized to list people" do
      before { unauthorize(:list, Person) }
      it "should not list people" do
        get "/people", params: { search: last_name1 }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW PERSON
  describe "GET /people/new" do
    before { authorize(:create, Person) }
    it "should render new person form" do
      get "/people/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create people" do
      before { unauthorize(:create, Person) }
      it "should not render new person form" do
        get "/people/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE PERSON
  describe "POST /people" do
    let(:name) { random_string }
    let(:date_of_birth) { Date.yesterday }
    let(:date_of_death) { Date.today }
    let(:tag1) { FactoryBot.create(:tag, :person) }
    let(:tag2) { FactoryBot.create(:tag, :person) }
    let(:nationalities) { Country.all.map(&:alpha2).sample(rand(2..7)) }
    before { authorize(:create, Person) }
    it "should create person" do
      expect {
        post "/people", params: {
          person: {
            name: name,
            date_of_birth: date_of_birth,
            date_of_death: date_of_death,
            approximate_dates: true,
            tag_ids: [tag1.id, tag2.id],
            nationalities: nationalities
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Person, :count).from(0).to(1)
      created_person = Person.first
      expect(created_person.name).to eq(name)
      expect(created_person.date_of_birth).to eq(date_of_birth)
      expect(created_person.date_of_death).to eq(date_of_death)
      expect(created_person.tag_ids).to contain_exactly(tag1.id, tag2.id)
      expect(created_person.nationalities).to contain_exactly(*nationalities)
    end
    context "when validation fails" do
      it "should not create person" do
        expect {
          post "/people", params: {
            person: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Person, :count)
      end
    end
    context "when user is not authorized to create people" do
      before { unauthorize(:create, Person) }
      it "should not authorize" do
        expect {
          post "/people", params: {
            person: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Person, :count)
      end
    end
  end

  # SHOW PERSON
  describe "GET /people/:id" do
    let!(:person) { FactoryBot.create(:person) }
    before { authorize(:read, person) }
    it "should show person" do
      get "/people/#{person.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
      expect(response.body).to include(person.name)
    end
    context "when user is not authorized to read the person" do
      before { unauthorize(:read, person) }
      it "should not show person" do
        get "/people/#{person.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(person.name)
      end
    end
  end

  # EDIT PERSON
  describe "GET /people/:id/edit" do
    let!(:person) { FactoryBot.create(:person) }
    before { authorize(:update, person) }
    it "should show edit people form" do
      get "/people/#{person.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit the person" do
      before { unauthorize(:update, person) }
      it "should not render edit person form" do
        get "/people/#{person.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE PERSON
  describe "PUT /people/:id" do
    let(:tag) { FactoryBot.create(:tag, :person) }
    let(:new_tag) { FactoryBot.create(:tag, :person) }
    let!(:person) { FactoryBot.create(:person, tag_ids: [tag.id]) }
    let(:new_name) { random_string }
    let(:new_date_of_birth) { Date.yesterday }
    let(:new_date_of_death) { Date.today }
    before { authorize(:update, person) }
    it "should update person" do
      put "/people/#{person.id}", params: {
        person: {
          name: new_name,
          date_of_birth: new_date_of_birth,
          date_of_death: new_date_of_death,
          approximate_dates: true,
          tag_ids: [new_tag.id]
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      person.reload
      expect(person.name).to eq(new_name)
      expect(person.date_of_birth).to eq(new_date_of_birth)
      expect(person.date_of_death).to eq(new_date_of_death)
      expect(person.tag_ids).to contain_exactly(new_tag.id)
    end
    context "when validation fails" do
      it "shoud not update person" do
        expect {
          put "/people/#{person.id}", params: {
            person: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { person.reload.name }
      end
    end
    context "when user is not authorized to update the person" do
      before { unauthorize(:update, person) }
      it "should not authorize" do
        expect {
          put "/people/#{person.id}", params: {
            person: {
              name: new_name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { person.reload.name }
      end
    end
  end

  # READ PERSON'S BIOGRAPHY
  describe "GET /people/:id/biography" do
    let(:person) { FactoryBot.create(:person) }
    before { authorize(:read, person) }
    it "should show person's biography" do
      get "/people/#{person.id}/biography"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:biography)
    end
    context "when user is not authorized to update the person" do
      before { unauthorize(:read, person) }
      it "should not show person's biography" do
        get "/people/#{person.id}/biography"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # SHOW PERSON'S HISTORY
  describe "GET /people/:id/history" do
    let!(:person) { FactoryBot.create(:person) }
    let!(:education) { FactoryBot.create(:education, student: person) }
    let!(:link) { FactoryBot.create(:link, linkable: education) }
    let!(:attachment) { FactoryBot.create(:attachment, attachable: education) }
    before { authorize :read, person }
    it "should show people's changes history" do
      get "/people/#{person.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read person" do
      before { unauthorize :read, person }
      it "should not show people's changes history" do
        get "/people/#{person.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT PERSON'S BIOGRAPHY
  describe "GET /people/:id/biography/edit" do
    let(:person) { FactoryBot.create(:person) }
    before { authorize(:update, person) }
    it "should show person's edit biography form" do
      get "/people/#{person.id}/biography/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form_biography)
    end
    context "when user is not authorized to update the person" do
      before { unauthorize(:update, person) }
      it "should not show person's edit biography form" do
        get "/people/#{person.id}/biography/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE PERSON'S BIOGRAPHY
  describe "PUT /people/:id/biography" do
    let(:person) { FactoryBot.create(:person) }
    let(:new_biography) { random_string }
    before { authorize(:update, person) }
    it "should update biography" do
      expect {
        put "/people/#{person.id}/biography", params: {
          person: { biography: new_biography }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update_biography)
      }.to change { person.reload.biography }.to(new_biography)
    end
    context "when user is not authorized to update the person" do
      before { unauthorize(:update, person) }
      it "should not authorize" do
        expect {
          put "/people/#{person.id}/biography"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { person.reload.biography }
      end
    end
  end

  # DESTROY PERSON
  describe "DELETE /people/:id" do
    let!(:person) { FactoryBot.create(:person) }
    before { authorize(:delete, person) }
    it "should delete person" do
      expect {
        delete "/people/#{person.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Person, :count).from(1).to(0)
    end
    context "when user is not authorized to destroy the person" do
      before { unauthorize(:delete, person) }
      it "should not delete person" do
        expect {
          delete "/people/#{person.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Person, :count)
      end
    end
  end
end
