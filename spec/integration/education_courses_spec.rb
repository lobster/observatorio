describe "education courses" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # EDUCATION COURSES INDEX
  describe "GET /education_courses" do
    let!(:suffix1) { "Facility" }
    let!(:suffix2) { "Nature" }

    let!(:education_course1) { FactoryBot.create(:education_course, name: "Education Course A #{suffix1}") }
    let!(:education_course2) { FactoryBot.create(:education_course, name: "Education Course B #{suffix1}") }
    let!(:education_course3) { FactoryBot.create(:education_course, name: "Education Course C #{suffix2}") }
    let!(:education_course4) { FactoryBot.create(:education_course, name: "Education Course D #{suffix2}") }
    let!(:education_course5) { FactoryBot.create(:education_course, name: "Education Course E #{suffix2}") }
    let!(:education_course6) { FactoryBot.create(:education_course, name: "Education Course F #{suffix2}") }
    let!(:education_course7) { FactoryBot.create(:education_course, name: "Education Course G #{suffix2}") }
    let!(:education_course8) { FactoryBot.create(:education_course, name: "Education Course H #{suffix2}") }
    let!(:education_course9) { FactoryBot.create(:education_course, name: "Education Course I #{suffix2}") }
    let!(:education_course10) { FactoryBot.create(:education_course, name: "Education Course J #{suffix2}") }
    let!(:education_course11) { FactoryBot.create(:education_course, name: "Education Course K #{suffix2}") }
    let!(:education_course12) { FactoryBot.create(:education_course, name: "Education Course L #{suffix2}") }

    before { authorize(:list, EducationCourse) }
    it "should list only 8 education courses per page" do
      get "/education_courses"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(education_course1.name)
      expect(response.body).to include(education_course2.name)
      expect(response.body).to include(education_course3.name)
      expect(response.body).to include(education_course4.name)
      expect(response.body).to include(education_course5.name)
      expect(response.body).to include(education_course6.name)
      expect(response.body).to include(education_course7.name)
      expect(response.body).to include(education_course8.name)
      expect(response.body).not_to include(education_course9.name)
      expect(response.body).not_to include(education_course10.name)
      expect(response.body).not_to include(education_course11.name)
      expect(response.body).not_to include(education_course12.name)
    end
    it "should limit search results to 8 education courses per page" do
      get "/education_courses", params: { search: suffix2 }
      expect(response.body).to include(education_course3.name)
      expect(response.body).to include(education_course4.name)
      expect(response.body).to include(education_course5.name)
      expect(response.body).to include(education_course6.name)
      expect(response.body).to include(education_course7.name)
      expect(response.body).to include(education_course8.name)
      expect(response.body).to include(education_course9.name)
      expect(response.body).to include(education_course10.name)
      expect(response.body).not_to include(education_course11.name)
      expect(response.body).not_to include(education_course12.name)
    end
    it "should only return 2 education courses" do
      get "/education_courses", params: { search: suffix1 }
      expect(response.body).to include(education_course1.name)
      expect(response.body).to include(education_course2.name)
      expect(response.body).not_to include(education_course3.name)
      expect(response.body).not_to include(education_course4.name)
      expect(response.body).not_to include(education_course5.name)
      expect(response.body).not_to include(education_course6.name)
      expect(response.body).not_to include(education_course7.name)
      expect(response.body).not_to include(education_course8.name)
      expect(response.body).not_to include(education_course9.name)
      expect(response.body).not_to include(education_course10.name)
      expect(response.body).not_to include(education_course11.name)
      expect(response.body).not_to include(education_course12.name)
    end
    context "when user is not authorized to list education courses" do
      before { unauthorize(:list, EducationCourse) }
      it "should not list education courses" do
        get "/education_courses", params: { query: education_course1.name }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        expect(response.body).not_to include(education_course1.name)
        expect(response.body).not_to include(education_course1.id)
      end
    end
  end

  # NEW EDUCATION COURSE FORM
  describe "GET /education_courses/new" do
    before { authorize :create, EducationCourse }
    it "should render new course form" do
      get "/education_courses/new"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to create courses" do
      before { unauthorize :create, EducationCourse }
      it "should not render new position form" do
        get "/education_courses/new"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EDUCATION COURSE
  describe "POST /education_courses" do
    let(:name) { random_string }
    before { authorize(:create, EducationCourse) }
    it "should create education course" do
      expect {
        post "/education_courses", params: {
          education_course: {
            name: name
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(EducationCourse, :count).from(0).to(1)
      created_course = EducationCourse.first
      expect(created_course.name).to eq(name)
    end
    context "when validation fails" do
      it "should not create education course" do
        expect {
          post "/education_courses", params: {
            education_course: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(EducationCourse, :count)
      end
    end
    context "when user is not authorized to create education courses" do
      before { unauthorize(:create, EducationCourse) }
      it "should not create education course" do
        expect {
          post "/education_courses", params: {
            education_course: {
              name: name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EducationCourse, :count)
      end
    end
  end

  # SHOW EDUCATION COURSE
  describe "GET /education_courses/:id" do
    let(:education_course) { FactoryBot.create(:education_course) }
    let!(:educations) { FactoryBot.create_list(:education, rand(5..10), course: education_course) }
    before { authorize(:read, education_course) }
    it "should show education course" do
      get "/education_courses/#{education_course.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read the education course" do
      before { unauthorize(:read, education_course) }
      it "should not show education course" do
        get "/education_courses/#{education_course.id}"
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  # SHOW EDUCATION COURSE'S HISTORY
  describe "GET /education_courses/:id/history" do
    let!(:education_course) { FactoryBot.create(:education_course) }
    before { authorize :read, education_course }
    it "should show education_courses's changes history" do
      get "/education_courses/#{education_course.id}/history"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:history)
    end
    context "when user is not authorized to read education_course" do
      before { unauthorize :read, education_course }
      it "should not show education_courses's changes history" do
        get "/education_courses/#{education_course.id}/history"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EDUCATION COURSE FORM
  describe "GET /education_courses/:id/edit" do
    let(:education_course) { FactoryBot.create(:education_course) }
    before { authorize :update, education_course }
    it "should render edit education_course form" do
      get "/education_courses/#{education_course.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to edit education_course" do
      before { unauthorize :update, education_course }
      it "should not render edit education_course form" do
        get "/education_courses/#{education_course.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EDUCATION COURSE
  describe "PUT /ajax/education_courses" do
    let!(:education_course) { FactoryBot.create(:education_course) }
    let(:new_name) { random_string }
    before { authorize(:update, education_course) }
    it "should update education course" do
      put "/education_courses/#{education_course.id}", params: {
        education_course: {
          name: new_name
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:update)
      education_course.reload
      expect(education_course.name).to eq(new_name)
    end
    context "when validation fails" do
      it "should not update education course" do
        expect {
          put "/education_courses/#{education_course.id}", params: {
            education_course: {
              name: ""
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change { education_course.reload.name }
      end
    end
    context "when user is not authorized to update the education course" do
      before { unauthorize(:update, education_course) }
      it "should not update the education course" do
        expect {
          put "/education_courses/#{education_course.id}", params: {
            education_course: {
              name: new_name
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change { education_course.reload.name }
      end
    end
  end

  # DESTROY EDUCATION COURSE
  describe "DELETE /education_courses/:id" do
    let!(:education_course) { FactoryBot.create(:education_course) }
    before { authorize(:delete, education_course) }
    it "should delete education course" do
      expect {
        delete "/education_courses/#{education_course.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(EducationCourse, :count).from(1).to(0)
    end
    context "when dependent education" do
      before { FactoryBot.create(:education, course: education_course) }
      it "should not delete education course" do
        expect {
          delete "/education_courses/#{education_course.id}"
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:error)
        }.not_to change(EducationCourse, :count)
      end
    end
    context "when user is not authorized to delete the education course" do
      before { unauthorize(:delete, education_course) }
      it "should not delete education course" do
        expect {
          delete "/education_courses/#{education_course.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(EducationCourse, :count)
      end
    end
  end
end
