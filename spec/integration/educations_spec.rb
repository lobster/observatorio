describe "educations" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # EDUCATIONS INDEX
  describe "GET /educations" do
    let!(:student) { FactoryBot.create(:person) }
    let!(:school) { FactoryBot.create(:organization, :school) }
    let!(:education1) { FactoryBot.create(:education, student: student, school: school) }
    let!(:education2) { FactoryBot.create(:education, student: student, school: school) }
    let!(:education3) { FactoryBot.create(:education, student: student) }
    let!(:education4) { FactoryBot.create(:education, school: school) }
    before do
      authorize :read, student
      authorize :read, school
    end
    it "should list educations of a student" do
      get "/educations", params: {
        education: { student_id: student.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(education1.id)
      expect(response.body).to include(education2.id)
      expect(response.body).to include(education3.id)
      expect(response.body).not_to include(education4.id)
    end
    it "should list students of a school" do
      get "/educations", params: {
        education: { school_id: school.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(education1.id)
      expect(response.body).to include(education2.id)
      expect(response.body).not_to include(education3.id)
      expect(response.body).to include(education4.id)
    end
    context "when neither student_id nor school_id given" do
      it "should respond bad request" do
        get "/educations"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to read student" do
      before { unauthorize :read, student }
      it "should respond forbidden" do
        get "/educations", params: {
          education: { student_id: student.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read school" do
      before { unauthorize :read, school }
      it "should respond forbidden" do
        get "/educations", params: {
          education: { school_id: school.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # NEW EDUCATION FORM
  describe "GET /educations/new" do
    let(:student) { FactoryBot.create(:person) }
    let(:school) { FactoryBot.create(:organization, :school) }
    before do
      authorize :update, student
      authorize :update, school
    end
    it "should render new education form for student" do
      get "/educations/new", params: {
        education: { student_id: student.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    it "should render new education form for school" do
      get "/educations/new", params: {
        education: { school_id: school.id }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when no student nor school is given" do
      it "should respond bad request" do
        get "/educations/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to update student" do
      before { unauthorize :update, student }
      it "should respond forbidden" do
        get "/educations/new", params: {
          education: { student_id: student.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update school" do
      before { unauthorize :update, school }
      it "should respond forbidden" do
        get "/educations/new", params: {
          education: { school_id: school.id }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE EDUCATION
  describe "POST /educations" do
    let(:student) { FactoryBot.create(:person) }
    let(:school) { FactoryBot.create(:organization, :school) }
    let(:education_course) { FactoryBot.create(:education_course) }
    let(:education_degree) { FactoryBot.create(:education_degree) }
    let(:from_date) { random_date }
    let(:to_date) { from_date + 1.day }
    let(:approximate_dates) { random_bool }
    before do
      authorize :update, student
      authorize :update, school
    end
    it "should create education" do
      expect {
        post "/educations", params: {
          education: {
            student_id: student.id,
            school_id: school.id,
            course_id: education_course.id,
            degree_id: education_degree.id,
            from_date: from_date,
            to_date: to_date,
            approximate_dates: approximate_dates
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Education, :count).from(0).to(1)
      created = Education.first
      expect(created.student).to eq(student)
      expect(created.school).to eq(school)
      expect(created.degree).to eq(education_degree)
      expect(created.course).to eq(education_course)
      expect(created.from_date).to be_within(0.01).of(from_date)
      expect(created.to_date).to be_within(0.01).of(to_date)
      expect(created.approximate_dates).to eq(approximate_dates)
    end
    context "when validation fails" do
      it "should not create education" do
        expect {
          post "/educations", params: {
            education: {
              student_id: student.id,
              school_id: school.id,
              from_date: from_date,
              approximate_dates: nil
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Education, :count)
      end
    end
    context "when user is not authorized to update student" do
      before { unauthorize :update, student }
      it "should not create education" do
        expect {
          post "/educations", params: {
            education: {
              student_id: student.id,
              school_id: school.id,
              course_id: education_course.id,
              degree_id: education_degree.id,
              from_date: from_date,
              to_date: to_date,
              approximate_dates: approximate_dates
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Education, :count)
      end
    end
    context "when user is not authorized to update school" do
      before { unauthorize :update, school }
      it "should not create education" do
        expect {
          post "/educations", params: {
            education: {
              student_id: student.id,
              school_id: school.id,
              course_id: education_course.id,
              degree_id: education_degree.id,
              from_date: from_date,
              to_date: to_date,
              approximate_dates: approximate_dates
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Education, :count)
      end
    end
  end

  # SHOW EDUCATION
  describe "GET /educations/:id" do
    let(:student) { FactoryBot.create(:person) }
    let(:school) { FactoryBot.create(:organization, :school) }
    let(:education) { FactoryBot.create(:education, student: student, school: school) }
    before do
      authorize :read, student
      authorize :read, school
    end
    it "should render education" do
      get "/educations/#{education.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when user is not authorized to read student" do
      before { unauthorize :read, student }
      it "should not render education" do
        get "/educations/#{education.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to read school" do
      before { unauthorize :read, school }
      it "should not render education" do
        get "/educations/#{education.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT EDUCATION FORM
  describe "GET /educations/:id/edit" do
    let(:student) { FactoryBot.create(:person) }
    let(:school) { FactoryBot.create(:organization, :school) }
    let(:education) { FactoryBot.create(:education, student: student, school: school) }
    before do
      authorize :update, student
      authorize :update, school
    end
    it "should render edit education form" do
      get "/educations/#{education.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update student" do
      before { unauthorize :update, student }
      it "should not render edit education form" do
        get "/educations/#{education.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when user is not authorized to update school" do
      before { unauthorize :update, school }
      it "should not render edit education form" do
        get "/educations/#{education.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE EDUCATION
  describe "PUT /educations/:id" do
    let!(:student) { FactoryBot.create(:person) }
    let!(:school) { FactoryBot.create(:organization, :school) }
    let!(:education) { FactoryBot.create(:education, student: student, school: school) }
    let!(:new_course) { FactoryBot.create(:education_course) }
    let!(:new_degree) { FactoryBot.create(:education_degree) }
    let!(:new_from_date) { random_date }
    let!(:new_to_date) { new_from_date + 1.day }
    let!(:new_approximate_dates) { education.approximate_dates.present? ? false : true }
    before do
      authorize :update, student
      authorize :update, school
    end
    it "should update education" do
      expect {
        put "/educations/#{education.id}", params: {
          education: {
            course_id: new_course.id,
            degree_id: new_degree.id,
            from_date: new_from_date,
            to_date: new_to_date,
            approximate_dates: new_approximate_dates
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update)
      }.not_to change(Education, :count)
      education.reload
      expect(education.course).to eq(new_course)
      expect(education.degree).to eq(new_degree)
      expect(education.from_date).to eq(new_from_date)
      expect(education.to_date).to eq(new_to_date)
      expect(education.approximate_dates).to eq(new_approximate_dates)
    end
    context "when validation fails" do
      it "should not update education" do
        put "/educations/#{education.id}", params: {
          education: {
            course_id: new_course.id,
            to_date: new_to_date,
            approximate_dates: nil
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:form)
        education.reload
        expect(education.course).not_to eq(new_course)
      end
    end
    context "when user is not authorized to update student" do
      before { unauthorize :update, student }
      it "should not update education" do
        put "/educations/#{education.id}", params: {
          education: {
            course_id: new_course.id,
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        education.reload
        expect(education.course).not_to eq(new_course)
      end
    end
    context "when user is not authorized to update school" do
      before { unauthorize :update, school }
      it "should not update education" do
        put "/educations/#{education.id}", params: {
          education: {
            course_id: new_course.id,
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
        education.reload
        expect(education.course).not_to eq(new_course)
      end
    end
  end

  # DESTROY EDUCATION
  describe "DELETE /educations/:id" do
    let!(:student) { FactoryBot.create(:person) }
    let!(:school) { FactoryBot.create(:organization, :school) }
    let!(:education) { FactoryBot.create(:education, student: student, school: school) }
    before do
      authorize :update, student
      authorize :update, school
    end
    it "should delete education" do
      expect {
        delete "/educations/#{education.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Education, :count).from(1).to(0)
    end
    context "when user is not authorized to update student" do
      before { unauthorize :update, student }
      it "should not delete education" do
        expect {
          delete "/educations/#{education.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Education, :count)
      end
    end
    context "when user is not authorized to update school" do
      before { unauthorize :update, school }
      it "should not delete education" do
        expect {
          delete "/educations/#{education.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Education, :count)
      end
    end
  end
end
