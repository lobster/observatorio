describe "pictures" do
  let(:user) { FactoryBot.create(:user, :confirmed) }

  before do
    sign_in(user)
    PaperTrail.request.whodunnit = user
  end

  # PICTURE INDEX
  describe "GET /pictures" do
    let!(:picturable) { FactoryBot.create(:person) }
    let!(:picture1) { FactoryBot.create(:picture) }
    let!(:picture2) { FactoryBot.create(:picture, picturable: picturable) }
    before { authorize :read, picturable }
    it "should render picture" do
      get "/pictures", params: {
        picture: {
          picturable_id: picturable.id,
          picturable_type: picturable.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index)
      expect(response.body).to include(picture2.id)
      expect(response.body).not_to include(picture1.id)
    end
    context "when picturable is an organization" do
      let!(:picturable) { FactoryBot.create(:organization) }
      it "should render picture" do
        get "/pictures", params: {
          picture: {
            picturable_id: picturable.id,
            picturable_type: picturable.class
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
        expect(response.body).to include(picture2.id)
        expect(response.body).not_to include(picture1.id)
      end
    end
    context "when user is not authorized to read picturable" do
      before { unauthorize :read, picturable }
      it "should respond forbidden" do
        get "/pictures", params: {
          picture: {
            picturable_id: picturable.id,
            picturable_type: picturable.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
    context "when person has no picture" do
      let!(:person_without_picture) { FactoryBot.create(:person) }
      before { authorize :read, person_without_picture }
      it "should render placeholder" do
        get "/pictures", params: {
          picture: {
            picturable_id: person_without_picture.id,
            picturable_type: person_without_picture.class
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
        expect(response.body).not_to include(picture2.id)
        expect(response.body).not_to include(picture1.id)
      end
    end
    context "when organization has no picture" do
      let!(:organization_without_picture) { FactoryBot.create(:organization) }
      before { authorize :read, organization_without_picture }
      it "should render placeholder" do
        get "/pictures", params: {
          picture: {
            picturable_id: organization_without_picture.id,
            picturable_type: organization_without_picture.class
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:index)
        expect(response.body).not_to include(picture2.id)
        expect(response.body).not_to include(picture1.id)
      end
    end
  end

  # NEW PICTURE FORM
  describe "GET /pictures/new" do
    let!(:picturable) { FactoryBot.create(:person) }
    before { authorize :update, picturable }
    it "should render new picture form" do
      get "/pictures/new", params: {
        picture: {
          picturable_id: picturable.id,
          picturable_type: picturable.class
        }
      }
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when picturable is not given" do
      it "should respond bad request" do
        get "/pictures/new"
        expect(response).to have_http_status(:bad_request)
        expect(response).to render_template(:bad_request)
      end
    end
    context "when user is not authorized to update the picturable" do
      before { unauthorize :update, picturable }
      it "should respond forbidden" do
        get "/pictures/new", params: {
          picture: {
            picturable_id: picturable.id,
            picturable_type: picturable.class
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # CREATE PICTURE
  describe "POST /pictures" do
    let!(:picturable) { FactoryBot.create(:person) }
    let(:file) do
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, "spec", "files", "meinhof.jpg"), "image/jpeg"
      )
    end
    let(:source_text) { random_string }
    let(:author) { random_string }
    let(:license) { random_string }
    before { authorize :update, picturable }
    it "should create picture" do
      expect {
        post "/pictures", params: {
          picture: {
            picturable_id: picturable.id,
            picturable_type: picturable.class,
            file: file,
            source_text: source_text,
            author: author,
            license: license
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:create)
      }.to change(Picture, :count).from(0).to(1)
      created = Picture.last
      expect(created.file.url).to be_present
      expect(created.source_text).to eq(source_text)
      expect(created.author).to eq(author)
      expect(created.license).to eq(license)
    end
    context "when URL is given" do
      let(:source_url) { random_url }
      let(:head) { double(success?: true, extension: ".jpg") }
      let(:get) do
        double(
          extension: ".jpg",
          file: File.new(File.join(Rails.root, "spec", "files", "meinhof.jpg"))
        )
      end
      before do
        allow(DOWNLOADER).to receive(:head)
                               .with(source_url)
                               .and_return(head)
        allow(DOWNLOADER).to receive(:get)
                               .with(source_url)
                               .and_return(get)
      end
      it "should create picture" do
        expect {
          post "/pictures", params: {
            picture: {
              picturable_id: picturable.id,
              picturable_type: picturable.class,
              source_url: source_url,
              author: author,
              license: license
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:create)
        }.to change(Picture, :count).from(0).to(1)
        created = Picture.last
        expect(created.file.url).to be_present
        expect(created.source_url).to eq(source_url)
        expect(created.author).to eq(author)
        expect(created.license).to eq(license)
      end
    end
    context "when validation fails" do
      it "should render form" do
        expect {
          post "/pictures", params: {
            picture: {
              picturable_id: picturable.id,
              picturable_type: picturable.class,
              source_text: source_text,
              author: author,
              license: license
            }
          }
          expect(response).to have_http_status(:ok)
          expect(response).to render_template(:form)
        }.not_to change(Picture, :count).from(0)
      end
    end
    context "when user is not authorized to update picturable" do
      before { unauthorize :update, picturable }
      it "should respond forbidden" do
        expect {
          post "/pictures", params: {
            picture: {
              picturable_id: picturable.id,
              picturable_type: picturable.class,
              file: file,
              source_text: source_text,
              author: author,
              license: license
            }
          }
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Picture, :count).from(0)
      end
    end
  end

  # SHOW PICTURE
  describe "GET /pictures/:id" do
    let!(:picturable) { FactoryBot.create(:person) }
    let!(:picture) { FactoryBot.create(:picture, picturable: picturable) }
    before { authorize :read, picturable }
    it "should render show popup" do
      get "/pictures/#{picture.id}"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:show)
    end
    context "when picturable is an organization" do
      let!(:picturable) { FactoryBot.create(:organization) }
      it "should render show popup" do
        get "/pictures/#{picture.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:show)
      end
    end
    context "when user is not authorized to read picturable" do
      before { unauthorize :read, picturable }
      it "should respond forbidden" do
        get "/pictures/#{picture.id}"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # EDIT PICTURE FORM
  describe "GET /pictures/:id/edit" do
    let!(:picturable) { FactoryBot.create(:person) }
    let!(:picture) { FactoryBot.create(:picture, picturable: picturable) }
    before { authorize :update, picturable }
    it "should render form" do
      get "/pictures/#{picture.id}/edit"
      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:form)
    end
    context "when user is not authorized to update picturable" do
      before { unauthorize :update, picturable }
      it "should respond forbidden" do
        get "/pictures/#{picture.id}/edit"
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # UPDATE PICTURE
  describe "PUT /pictures/:id" do
    let!(:picturable) { FactoryBot.create(:person) }
    let!(:picture) { FactoryBot.create(:picture, picturable: picturable) }
    let(:new_file) do
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, "spec", "files", "meinhof.jpg"), "image/jpeg"
      )
    end
    let(:new_source) { random_string }
    let(:new_author) { random_string }
    let(:new_license) { random_string }
    before { authorize :update, picturable }
    it "should update picture" do
      expect {
        put "/pictures/#{picture.id}", params: {
          picture: {
            file: new_file,
            source_text: new_source,
            author: new_author,
            license: new_license
          }
        }
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:update)
      }.not_to change(Picture, :count)
      picture = Picture.first
      expect(picture.source_text).to eq(new_source)
      expect(picture.author).to eq(new_author)
      expect(picture.license).to eq(new_license)
    end
    context "when user is not authorized to update picturable" do
      before { unauthorize :update, picturable }
      it "should respond forbidden" do
        put "/pictures/#{picture.id}", params: {
          picture: {
            file: new_file,
            source_text: new_source,
            author: new_author,
            license: new_license
          }
        }
        expect(response).to have_http_status(:forbidden)
        expect(response).to render_template(:forbidden)
      end
    end
  end

  # DESTROY PICTURE
  describe "DELETE /pictures/:id" do
    let!(:picturable) { FactoryBot.create(:person) }
    let!(:picture) { FactoryBot.create(:picture, picturable: picturable) }
    before { authorize :update, picturable }
    it "should delete picture" do
      expect {
        delete "/pictures/#{picture.id}"
        expect(response).to have_http_status(:ok)
        expect(response).to render_template(:destroy)
      }.to change(Picture, :count).from(1).to(0)
    end
    context "when user is not authorized to update picturable" do
      before { unauthorize :update, picturable }
      it "should not delete picture" do
        expect {
          delete "/pictures/#{picture.id}"
          expect(response).to have_http_status(:forbidden)
          expect(response).to render_template(:forbidden)
        }.not_to change(Picture, :count).from(1)
      end
    end
  end
end
