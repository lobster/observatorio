[![build status](https://framagit.org/lobster/observatorio/badges/develop/build.svg)](https://framagit.org/lobster/observatorio/commits/develop)
[![coverage report](https://framagit.org/lobster/observatorio/badges/develop/coverage.svg)](https://lobster.frama.io/observatorio/coverage)

# OBSERVATÓRIO

Observatório is a [free and open-source](https://en.wikipedia.org/wiki/Free_and_open-source_software) platform for relational, sourced and geo-referenced structuring of [prosopographical](https://en.wikipedia.org/wiki/Prosopography), organizational and event-driven information. Designed for social sciences use but also appropriate to journalism or any field which requires inquiring into social reality.

Observatório communicates with the following other systems:

##### PostgreSQL

[PostgreSQL](https://www.postgresql.org/) is a free and open-source [object-relational database](https://en.wikipedia.org/wiki/Object-relational_database) management system.

Observatório uses PostgreSQL to store all structured data.

##### Nominatim

[Nominatim](https://nominatim.openstreetmap.org/) is a free and open-source search engine for [OpenStreetMap](https://en.wikipedia.org/wiki/OpenStreetMap) data.

Observatório uses Nominatim API on location search features and for internal [reverse geocoding](https://en.wikipedia.org/wiki/Reverse_geocoding).

##### OpenStack Swift

Swift is the [object storage](https://en.wikipedia.org/wiki/Object_storage) component of [OpenStack](https://en.wikipedia.org/wiki/OpenStack), a free and open-source software platform for cloud computing.

Observatório stores all files uploaded by users (except public images) using a remote OpenStack server.

##### Mapbox

[Mapbox](https://www.mapbox.com/) is a commercial online service of maps rendering.

All maps displayed on Obsevatório's user interface are powered by Mapbox.

##### Cloudinary

[Cloudinary](https://cloudinary.com/) is a commercial online service of image and video storage.

Observatório uses Cloudinary to store pictures so they can be resized & transformed on the fly.

## DEPLOYMENT

See the [Heroku deployment guide](https://framagit.org/lobster/observatorio/blob/develop/docs/DEPLOY_HEROKU.md).

## CONTRIBUTING

See the [development guide](https://framagit.org/lobster/observatorio/blob/develop/docs/DEVELOPMENT_GUIDE.md).

## LICENSE

[GNU Affero General Public License V3](https://www.gnu.org/licenses/agpl-3.0.en.html)
