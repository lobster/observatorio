class AddLabelToLinks < ActiveRecord::Migration[5.0]
  def change
    add_column :links, :label, :text
    reversible do |dir|
      dir.up do
        execute "UPDATE links SET label = 'External link'"
      end
    end
    change_column_null :links, :label, false
  end
end
