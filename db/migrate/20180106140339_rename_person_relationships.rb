class RenamePersonRelationships < ActiveRecord::Migration[5.0]
  def change
    rename_table  :person_relationships, :relationships
    rename_column :relationships, :person_id, :with_id
    reversible do |dir|
      dir.up do
        execute "UPDATE versions SET item_type = 'Relationship' WHERE item_type = 'PersonRelationship'"
      end
      dir.down do
        execute "UPDATE versions SET item_type = 'PersonRelationship' WHERE item_type = 'Relationship'"
      end
    end
  end
end
