class RenameCategoriesIntoOrganizationCategories < ActiveRecord::Migration[5.0]
  def up
    remove_index :categories, [:name, :type]
    remove_index :categories, :type
    remove_column :categories, :type
    rename_table :categories, :organization_categories
    add_index :organization_categories, :name, unique: true
  end

  def down
    remove_index :organization_categories, :name
    rename_table :organization_categories, :categories
    add_column :categories, :type, :text
    add_index :categories, [:name, :type], unique: true
    add_index :categories, :type
  end
end
