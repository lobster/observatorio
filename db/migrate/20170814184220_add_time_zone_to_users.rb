class AddTimeZoneToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :time_zone,          :text
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE users SET time_zone = 'Brasilia'
        SQL
      end
    end
    change_column_null :users, :time_zone,          false
  end
end
