class FixMembershipIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :memberships, :organization_id
    add_index :memberships, [:organization_id, :member_id, :position_id, :location_id], name: "index_to_memberships_unique_partial"
  end
end
