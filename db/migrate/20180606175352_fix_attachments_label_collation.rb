class FixAttachmentsLabelCollation < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      dir.up { change_column :attachments, :label, :text, collation: I18n.locale.to_s.gsub("-", "_") }
      dir.down { change_column :attachments, :label, :text }
    end
  end
end
