class FixDepartmetsUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :departments, [:organization_id, :name]
    add_index :departments, "organization_id, lower(name) varchar_pattern_ops", unique: true, name: "index_to_departments_on_organization_and_name_case_insensitive"
  end
end
