class AddApproximateDatesToDateBoundaries < ActiveRecord::Migration[5.0]
  def change
    [:memberships, :educations, :relationships, :people].each do |table|
      add_column table, :approximate_dates, :boolean
    end
  end
end
