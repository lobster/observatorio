class RenameOwnershipsToOrganizationOwners < ActiveRecord::Migration[5.0]
  def change
    rename_table :ownerships, :organizations_owners
  end
end
