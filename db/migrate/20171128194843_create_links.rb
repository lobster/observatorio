class CreateLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :links, id: :uuid do |t|
      t.timestamps               null: false
      t.text       :url,         null: false
      t.uuid       :object_id,   null: false
      t.text       :object_type, null: false
    end
    add_index :links, [:object_id, :object_type]
  end
end
