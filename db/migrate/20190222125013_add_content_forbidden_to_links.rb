class AddContentForbiddenToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :content_forbidden, :boolean
  end
end
