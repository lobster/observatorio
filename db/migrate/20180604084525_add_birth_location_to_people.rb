class AddBirthLocationToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :birth_location_id, :uuid
  end
end
