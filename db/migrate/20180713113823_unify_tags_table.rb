class UnifyTagsTable < ActiveRecord::Migration[5.0]
  def change
    create_table :tags, id: :uuid do |t|
      t.timestamps null: false
      t.text       :name,             null: false
      t.integer    :display_priority, null: false
      t.text       :type,             null: false
    end
    add_index :tags, "lower(name) varchar_pattern_ops,type", unique: true
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO tags (id, created_at, updated_at, type, name, display_priority)
            SELECT id, created_at, updated_at, 'PersonTag', name, display_priority
            FROM person_tags
        SQL
        execute <<-SQL
          INSERT INTO tags (id, created_at, updated_at, type, name, display_priority)
            SELECT id, created_at, updated_at, 'OrganizationTag', name, display_priority
            FROM organization_tags
        SQL
        execute <<-SQL
          INSERT INTO tags (id, created_at, updated_at, type, name, display_priority)
            SELECT id, created_at, updated_at, 'EventTag', name, display_priority
            FROM event_tags
        SQL
      end
      dir.down do
        execute <<-SQL
          INSERT INTO person_tags (id, created_at, updated_at, name, display_priority)
            SELECT id, created_at, updated_at, name, display_priority
            FROM tags
            WHERE type = 'PersonTag'
        SQL
        execute <<-SQL
          INSERT INTO organization_tags (id, created_at, updated_at, name, display_priority)
            SELECT id, created_at, updated_at, name, display_priority
            FROM tags
            WHERE type = 'OrganizationTag'
        SQL
        execute <<-SQL
          INSERT INTO event_tags (id, created_at, updated_at, name, display_priority)
            SELECT id, created_at, updated_at, name, display_priority
            FROM tags
            WHERE type = 'EventTag'
        SQL
      end
    end
    rename_table :people_person_tags, :people_tags
    rename_table :organizations_organization_tags, :organizations_tags
    rename_table :events_event_tags, :events_tags
    [:person_tags, :organization_tags, :event_tags].each do |old_tag_table|
      remove_index old_tag_table, "lower(name) varchar_pattern_ops"
      drop_table old_tag_table, id: :uuid do |t|
        t.timestamps                    null: false
        t.text       :name,             null: false
        t.integer    :display_priority, null: false
      end
    end
  end
end
