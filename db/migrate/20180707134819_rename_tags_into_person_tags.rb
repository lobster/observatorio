class RenameTagsIntoPersonTags < ActiveRecord::Migration[5.0]
  def change
    rename_table :tags, :person_tags
    rename_table :people_tags, :people_person_tags
    rename_column :people_person_tags, :tag_id, :person_tag_id
  end
end
