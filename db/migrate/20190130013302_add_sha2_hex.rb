class AddSha2Hex < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :content_sha2_hex, :text
    add_column :attachments, :file_sha2_hex, :text
    add_column :imports, :file_sha2_hex, :text
  end
end
