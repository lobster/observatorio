class AddPrimaryKeyToPeopleTags < ActiveRecord::Migration[5.1]
  def change
    add_column :people_tags, :id, :uuid, default: "uuid_generate_v4()", primary_key: true
  end
end
