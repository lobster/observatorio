class ChangeLinksContentExtensionNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :links, :content_extension, true
  end
end
