class CreateImports < ActiveRecord::Migration[5.0]
  def change
    create_table :imports, id: :uuid do |t|
      t.timestamps                      null: false
      t.text       :file,               null: false
      t.text       :file_extension,     null: false
      t.integer    :file_size,          null: false, limit: 8
      t.text       :file_encryption_iv, null: false
      t.text       :status,             null: false
      t.uuid       :user_id,            null: false
      t.json       :logs,               null: false
    end
  end
end
