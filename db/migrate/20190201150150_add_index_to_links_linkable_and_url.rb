class AddIndexToLinksLinkableAndUrl < ActiveRecord::Migration[5.2]
  def change
    add_index :links, "linkable_id, linkable_type, lower(url) varchar_pattern_ops", {
      unique: true, name: "index_links_on_linkable_and_url_case_insensitive"
    }
  end
end
