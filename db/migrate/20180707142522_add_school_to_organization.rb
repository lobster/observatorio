class AddSchoolToOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :school, :boolean
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE organizations SET school = organization_categories.school
            FROM organization_categories
            WHERE organizations.category_id = organization_categories.id
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE organization_categories SET school = organizations.school
            FROM organizations
            WHERE organizations.category_id = organization_categories.id
        SQL
      end
    end
    change_column_null :organizations, :school, false
    remove_column :organization_categories, :school, :boolean
  end
end
