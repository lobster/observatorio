class RenamePersonTagIdOnPeopleTags < ActiveRecord::Migration[5.1]
  def change
    rename_column :people_tags, :person_tag_id, :tag_id
  end
end
