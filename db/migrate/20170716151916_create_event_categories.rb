class CreateEventCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :event_categories, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false, unique: true
      t.integer    :display_priority, null: false
    end
    add_column :events, :category_id, :uuid
    reversible do |dir|
      dir.up do
        execute <<-SQL
          INSERT INTO event_categories (id, name, display_priority, created_at, updated_at)
            VALUES (uuid_generate_v4(), 'other', 1, now(), now());
          UPDATE events
            SET category_id = event_categories.id
            FROM event_categories
            WHERE event_categories.name = 'other';
        SQL
      end
    end
    change_column_null :events, :category_id, false
  end
end
