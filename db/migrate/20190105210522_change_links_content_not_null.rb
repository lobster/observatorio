class ChangeLinksContentNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :links, :content, false
    change_column_null :links, :content_extension, false
    change_column_null :links, :content_size, false
    change_column_null :links, :content_encryption_iv, false
    change_column_null :links, :visited_at, false
  end
end
