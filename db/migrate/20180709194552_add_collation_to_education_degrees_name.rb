class AddCollationToEducationDegreesName < ActiveRecord::Migration[5.0]
  def up
    change_column :education_degrees, :name, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :education_degrees, :name, :text
  end
end
