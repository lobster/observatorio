class RenameRelationshipTypes < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      dir.up do
        execute "UPDATE relationships SET type = 'Relationship::Child'   WHERE type = 'Child'"
        execute "UPDATE relationships SET type = 'Relationship::Parent'  WHERE type = 'Parent'"
        execute "UPDATE relationships SET type = 'Relationship::Partner' WHERE type = 'Partner'"
        execute "UPDATE relationships SET type = 'Relationship::Sibling' WHERE type = 'Sibling'"
        # PaperTrail versions
        execute "UPDATE versions SET item_type = 'Relationship::Child'   WHERE item_type = 'Child'"
        execute "UPDATE versions SET item_type = 'Relationship::Parent'  WHERE item_type = 'Parent'"
        execute "UPDATE versions SET item_type = 'Relationship::Partner' WHERE item_type = 'Partner'"
        execute "UPDATE versions SET item_type = 'Relationship::Sibling' WHERE item_type = 'Sibling'"
      end
      dir.down do
        execute "UPDATE relationships SET type = 'Child'   WHERE type = 'Relationship::Child'"
        execute "UPDATE relationships SET type = 'Parent'  WHERE type = 'Relationship::Parent'"
        execute "UPDATE relationships SET type = 'Partner' WHERE type = 'Relationship::Partner'"
        execute "UPDATE relationships SET type = 'Sibling' WHERE type = 'Relationship::Sibling'"
        # PaperTrail versions
        execute "UPDATE versions SET item_type = 'Child'   WHERE item_type = 'Relationship::Child'"
        execute "UPDATE versions SET item_type = 'Parent'  WHERE item_type = 'Relationship::Parent'"
        execute "UPDATE versions SET item_type = 'Partner' WHERE item_type = 'Relationship::Partner'"
        execute "UPDATE versions SET item_type = 'Sibling' WHERE item_type = 'Relationship::Sibling'"
      end
    end
  end
end
