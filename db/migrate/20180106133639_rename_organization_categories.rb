class RenameOrganizationCategories < ActiveRecord::Migration[5.0]
  def change
    rename_table :organization_categories, :categories
    add_column   :categories, :type, :text
    reversible do |dir|
      dir.up do
        execute "UPDATE categories SET type = 'OrganizationCategory'"
      end
    end
    change_column_null :categories, :type, false
    add_index          :categories, :type
  end
end
