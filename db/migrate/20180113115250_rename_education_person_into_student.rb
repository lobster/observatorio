class RenameEducationPersonIntoStudent < ActiveRecord::Migration[5.0]
  def change
    rename_column :educations, :person_id, :student_id
  end
end
