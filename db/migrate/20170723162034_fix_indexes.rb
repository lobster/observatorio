class FixIndexes < ActiveRecord::Migration[5.0]
  def change
    remove_index :events,                  [:name, :date]
    add_index    :events,                  [:date, :name],        unique: true
    add_index    :event_categories,        :name,                 unique: true
    add_index    :organization_categories, :name,                 unique: true
    add_index    :organizations,           [:category_id, :name], unique: true
    add_index    :participation_roles,     :name,                 unique: true
  end
end
