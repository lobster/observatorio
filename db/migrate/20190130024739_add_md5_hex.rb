class AddMd5Hex < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :content_md5_hex, :text
    add_column :attachments, :file_md5_hex, :text
    add_column :imports, :file_md5_hex, :text
  end
end
