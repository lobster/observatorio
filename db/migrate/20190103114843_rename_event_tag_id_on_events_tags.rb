class RenameEventTagIdOnEventsTags < ActiveRecord::Migration[5.1]
  def change
    rename_column :events_tags, :event_tag_id, :tag_id
  end
end
