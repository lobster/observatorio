class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people, id: :uuid do |t|
      t.timestamps              null: false
      t.text       :first_name, null: false
      t.text       :last_name,  null: false
    end
    add_index :people, [:first_name, :last_name], unique: true
  end
end
