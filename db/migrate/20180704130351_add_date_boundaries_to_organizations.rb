class AddDateBoundariesToOrganizations < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :date_of_foundation, :date
    add_column :organizations, :date_of_extinction, :date
    add_column :organizations, :approximate_dates,  :boolean
  end
end
