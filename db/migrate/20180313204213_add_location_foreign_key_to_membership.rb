class AddLocationForeignKeyToMembership < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :location_id, :uuid
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE memberships SET location = organizations.headquarters
            FROM organizations
            WHERE organizations.id = memberships.organization_id AND
                  memberships.location IS NULL AND
                  organizations.headquarters IS NOT NULL
        SQL
      end
    end
    rename_column :memberships, :location, :location_old
  end
end
