class RenameCoursesToEducationCourses < ActiveRecord::Migration[5.0]
  def change
    rename_table :courses, :education_courses
    remove_column :education_courses, :school_id, :uuid
    remove_column :education_courses, :degree_id, :uuid
  end
end
