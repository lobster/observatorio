class CreateOrganizationCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_categories, id: :uuid do |t|
      t.timestamps                    null: false
      t.text       :name,             null: false, unique: true
      t.integer    :display_priority, null: false
    end
    add_column :organizations, :category_id, :uuid
    change_column_null :organizations, :category_id, false
    remove_column :organizations, :category, :text
  end
end
