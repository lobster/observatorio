class AddCollationToDepartmentsName < ActiveRecord::Migration[5.0]
  def up
    change_column :departments, :name, :text, collation: I18n.locale.to_s.gsub("-", "_")
  end

  def down
    change_column :departments, :name, :text
  end
end
