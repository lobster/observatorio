class FixCoursesUniqueIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :courses, [:name, :school_id, :degree_id]
    add_index :courses, "school_id, degree_id, lower(name) varchar_pattern_ops", unique: true, name: "index_to_courses_on_school_and_degree_and_name_case_insensitive"
  end
end
