class AddCollationToTextColumns < ActiveRecord::Migration[5.0]
  def change
    reversible do |dir|
      dir.up do
        change_column :people,                  :first_name, :text, collation: I18n.locale.to_s.gsub("-", "_")
        change_column :people,                  :last_name,  :text, collation: I18n.locale.to_s.gsub("-", "_")
        change_column :organizations,           :name,       :text, collation: I18n.locale.to_s.gsub("-", "_")
        change_column :organization_categories, :name,       :text, collation: I18n.locale.to_s.gsub("-", "_")
        change_column :organization_positions,  :name,       :text, collation: I18n.locale.to_s.gsub("-", "_")
      end
      dir.down do
        change_column :people,                  :first_name, :text
        change_column :people,                  :last_name,  :text
        change_column :organizations,           :name,       :text
        change_column :organization_categories, :name,       :text
        change_column :organization_positions,  :name,       :text
      end
    end
  end
end
