class CreatePersonRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :person_relationships, id: :uuid do |t|
      t.timestamps             null: false
      t.uuid       :owner_id,  null: false
      t.uuid       :person_id, null: false
      t.date       :since_date
      t.date       :until_date
      t.text       :type,      null: false
    end
    add_index :person_relationships, [:owner_id, :person_id], unique: true
  end
end
