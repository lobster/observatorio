class AddNationalitiesToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :nationalities, :text, array: true
  end
end
