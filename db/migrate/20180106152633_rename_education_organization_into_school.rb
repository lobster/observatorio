class RenameEducationOrganizationIntoSchool < ActiveRecord::Migration[5.0]
  def change
    rename_column :educations, :organization_id, :school_id
  end
end
