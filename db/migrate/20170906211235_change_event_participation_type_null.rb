class ChangeEventParticipationTypeNull < ActiveRecord::Migration[5.0]
  def change
    change_column_null :event_participations, :type, true
  end
end
