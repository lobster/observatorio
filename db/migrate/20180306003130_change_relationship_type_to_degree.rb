class ChangeRelationshipTypeToDegree < ActiveRecord::Migration[5.0]
  def change
    add_column :relationships, :degree, :text
    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE relationships SET degree = 'child' WHERE type = 'Relationship::Child';
          UPDATE relationships SET degree = 'parent' WHERE type = 'Relationship::Parent';
          UPDATE relationships SET degree = 'partner' WHERE type = 'Relationship::Partner';
          UPDATE relationships SET degree = 'sibling' WHERE type = 'Relationship::Sibling';
        SQL
      end
      dir.down do
        execute <<-SQL
          UPDATE relationships SET type = 'Relationship::Child' WHERE degree = 'child';
          UPDATE relationships SET type = 'Relationship::Parent' WHERE degree = 'parent';
          UPDATE relationships SET type = 'Relationship::Partner' WHERE degree = 'partner';
          UPDATE relationships SET type = 'Relationship::Sibling' WHERE degree = 'sibling';
        SQL
      end
    end
    remove_column :relationships, :type, :text
  end
end
