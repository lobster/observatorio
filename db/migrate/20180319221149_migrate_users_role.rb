class MigrateUsersRole < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :role, :text
    create_table :user_groups_users, id: false do |t|
      t.timestamps                 null: false
      t.uuid       :user_id,       null: false
      t.uuid       :user_group_id, null: false
    end
    add_index :user_groups_users, :user_id
    add_index :user_groups_users, :user_group_id
  end
end
