class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses, id: :uuid do |t|
      t.timestamps                   null: false
      t.text      :name,             null: false
      t.integer   :display_priority, null: false
      t.uuid      :school_id,        null: false
      t.uuid      :degree_id,        null: false
    end
    add_index :courses, [:name, :school_id, :degree_id], unique: true
  end
end
