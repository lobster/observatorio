class AddTypeToImports < ActiveRecord::Migration[5.2]
  def change
    add_column :imports, :type, :text
    reversible do |dir|
      dir.up do
        execute "UPDATE imports SET type = 'MembershipsImport'"
      end
    end
    change_column_null :imports, :type, false
  end
end
