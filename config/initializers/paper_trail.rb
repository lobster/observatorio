require 'paper_trail/frameworks/active_record/models/paper_trail/version'

PaperTrail.config.track_associations = true

module PaperTrail
  class Version < ActiveRecord::Base
    belongs_to :user, class_name: "User", foreign_key: "whodunnit"
  end
end
