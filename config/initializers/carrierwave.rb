require "carrierwave"

CarrierWave.configure do |config|
  config.cache_storage = :file
  if APP_CONFIG["local_storage"]
    config.fog_provider = "fog/local"
    config.fog_directory = "files"
    config.fog_credentials = {
      provider: "local",
      local_root: File.join(Rails.root)
    }
  else
    config.fog_directory = APP_CONFIG["openstack"].delete("swift")["container"]
    config.fog_credentials = APP_CONFIG["openstack"].merge(provider: "openstack")
  end
end
