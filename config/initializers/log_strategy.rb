class LogStrategy
  class << self
    def logger
      @logger ||= logger_instance
    end

    private

    def logger_instance
      logger = ActiveSupport::Logger.new(Rails.env.test? ? File::NULL : STDOUT)
      logger.level = logger_level
      logger
    end

    def logger_level
      case APP_CONFIG["log_level"]
      when "error"
        Logger::ERROR
      when "warn" || "warning"
        Logger::WARN
      when "info"
        Logger::INFO
      when "debug"
        Logger::DEBUG
      else
        Logger::INFO
      end
    end
  end
end
