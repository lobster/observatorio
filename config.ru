# This file is used by Rack-based servers to start the application.

allowed_prefix = "OBSERVATORIO_"

ENV.keys.each do |key|
  next unless key.start_with?(allowed_prefix)
  ENV[key[allowed_prefix.size..-1]] = ENV[key]
end

require_relative 'config/environment'

run Rails.application
